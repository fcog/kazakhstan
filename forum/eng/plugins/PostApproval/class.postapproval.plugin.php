<?php if (!defined('APPLICATION')) exit();

// Define the plugin:
$PluginInfo['PostApproval'] = array(
   'Description' => 'Requires all new discussions and comments to be approved by moderators',
   'Version' => '1.0b2',
   'Author' => "Jonathan Pautsch",
   'AuthorEmail' => 'ozone@secondwindprojects.com',
   'AuthorUrl' => 'http://secondwindprojects.com/ozone'
);

class PostApproval extends Gdn_Plugin
{
	public function Setup()
	{
		$this->Structure();
	}

	// Add fields to database to track approval status
	public function Structure()
	{
		Gdn::Structure()
			->Table('Discussion')
			->Column('Hidden', array('Yes', 'No'), 'Yes')
			->Set();
		
		// Number of comments in a discussion that are awaiting approval
		Gdn::Structure()
			->Table('Discussion')
			->Column('CountPendingComments', 'int', NULL)
			->Set();

		Gdn::Structure()
			->Table('Comment')
			->Column('Hidden', array('Yes', 'No'), 'Yes')
			->Set();
		 
		// Reset all discussions to be visible when plugin is first setup
		Gdn::SQL()->Update('Discussion')
			->Set('Hidden', 'No')
			->Put();
			
		// Reset all comments to be visible when plugin is first setup
		Gdn::SQL()->Update('Comment')
			->Set('Hidden', 'No')
			->Put();
	}
	
	// Add CSS for rendering pending labels and notices
	/*public function Base_Render_Before($Sender)
	{
		$Sender->AddCSSFile('plugins/PostApproval/design/postapproval.css');
	}*/

	// Create a preference to allow moderators to elect to receive emails when a new post requires approval
	public function ProfileController_AfterPreferencesDefined_Handler($Sender)
	{
		if (Gdn::Session()->CheckPermission('Vanilla.Categories.Manage')) // Generally moderators have this permission
			$Sender->Preferences['Notifications']['Email.PostApproval'] = array(T('Notify me when a new post requires approval.'), 'Meta');

	}
	
	// Filter discussions
	public function DiscussionModel_BeforeGet_Handler($Sender)
	{	
		$Session = Gdn::Session();
		$UserID = $Session->UserID > 0 ? $Session->UserID : 0;
		
		if( !$Session->CheckPermission('Vanilla.Categories.Manage') ) // Moderators can see hidden posts
		{
			$Sender->SQL->BeginWhereGroup()
						->Where('d.Hidden','No')
						->OrWhere('d.InsertUserID',$UserID) // Authors can see their own hidden posts
						->EndWhereGroup();
		}
	}
	
	// Comment count will not include hidden posts
	public function DiscussionModel_BeforeGetCount_Handler($Sender)
	{
		$Sender->EventArguments['Wheres']['d.Hidden'] = 'No';
	}
	
	// Label hidden discussions in the index so moderators can spot them
	public function Base_BeforeDiscussionMeta_Handler($Sender, $Args)
	{
		$Discussion = $Args['Discussion'];

		if (GetValue('Hidden', $Discussion) != 'Yes')
			return;

		$Text = 'Pending';
		$Title = ' title="'.T("This post requires moderator approval").'"';

		echo '<span class="Tag Pending"' . $Title . '>' . $Text . '</span> ';
	}
	
	// Send email notifications to moderators who have elected to be notified when a new post requires approval
	public function PostController_AfterDiscussionSave_Handler($Sender)
	{	
		$FormValues = $Sender->Form->FormValues();
		$IsModerator = Gdn::Session()->CheckPermission('Vanilla.Categories.Manage');
		if ( ($FormValues['DiscussionID'] == 0 || !isset($FormValues['DiscussionID'])) && !$FormValues['Announce'] && !$IsModerator)
		{
		
		$SQL = Gdn::SQL();
		
		// List of users to be notified
		$SqlUsers = $SQL->Select('m.UserID, u.Name, u.Email')
		   ->From('UserMeta m')
		   ->Join('User u', 'm.UserID = u.UserID')
		   ->Where('u.Deleted', '0')
		   ->Where('m.Name', 'Preferences.Email.PostApproval') 
		   ->Where('m.Value', '1') 
		   ->Get();		
		
		$Users = array();
		while($User = $SqlUsers->NextRow(DATASET_TYPE_ARRAY))
		{
			$Users[] = array( $User['UserID'], $User['Name'], $User['Email']);
		}
		
		$Discussion = $Sender->EventArguments['Discussion'];
		$DiscussionID = GetValue('DiscussionID', $Discussion);
		
		// Build query to allow moderators to approve a post
		$Query = http_build_query(array('discussionid' => GetValue('DiscussionID', $Discussion)));
		$url = Url("/discussion/approve/discussion&{$Query}", TRUE);
		
		$InsertUser = $SQL->Select('Name')
		       ->From('User')
		       ->Where('UserID', GetValue('InsertUserID', $Discussion)) 
		       ->Get()->FirstRow(DATASET_TYPE_ARRAY);
		
		$Subject = 'New discussion requires approval';
		$ActivitySubHead = $InsertUser['Name'] . ' posted a new discussion that requires approval.';
		$Story = "[" . GetValue('Name', $Discussion) . "]\n" . Gdn_Format::Text(GetValue('Body', $Discussion));
		$Story .= "\n\nClick this link to APPROVE the discussion:\n" . $url;

		foreach ($Users as $User) {
			$Email = new Gdn_Email();
			$Email->Subject(sprintf(T('[%1$s] %2$s'), Gdn::Config('Garden.Title'), $Subject));
			$Email->To($User[2], $User[1]);
			//$Email->From(Gdn::Config('Garden.SupportEmail'), Gdn::Config('Garden.SupportName'));
			$Email->Message(
				sprintf(
				T('EmailStoryNotification'),
				$ActivitySubHead,
				Url("/discussion/{$DiscussionID}", TRUE),
				$Story
				)
			);

			try
			{
				$Email->Send();
			}
			catch (Exception $ex)
			{
				echo $ex;
				die();
				// Don't do anything with the exception.
			}
		}
		}
		else if ( $FormValues['Announce'] || $IsModerator )	// Announcements bypass queue
		{
			$Discussion = $Sender->EventArguments['Discussion'];
			$DiscussionID = GetValue('DiscussionID', $Discussion);
			Gdn::SQL()->Put('Discussion', array('Hidden' => 'No'), array('DiscussionID' => $DiscussionID));
		}
	}
	
	// Filter commments
	public function CommentModel_AfterCommentQuery_Handler($Sender, $Args)
	{
		$Session = Gdn::Session();
		$UserID = $Session->UserID > 0 ? $Session->UserID : 0;
		
		// Hide unapproved comments from non-moderators
		if( !$Session->CheckPermission('Vanilla.Categories.Manage') )
		{
			$Sender->SQL->BeginWhereGroup()
						->Where('c.Hidden','No')
						->OrWhere('c.InsertUserID',$UserID) // Authors can see their own hidden comments
						->EndWhereGroup();
		}
	}
	
	// Comment count will not include hidden comments
	public function CommentModel_BeforeGetCount_Handler($Sender)
	{
		$Sender->SQL->Where('Hidden','No');
	}
	
	// Render a label indicating the post requires approval and a link to approve it
	// Using the PostController allows this to be rendered with the javascript when a new comment is posted
	public function PostController_CommentInfo_Handler($Sender, $Args)
	{
		$Comment = GetValue('Comment', $Args);

		if ( !(GetValue('Hidden', $Comment) == 'Yes') )
			return;
		 
		// Check to see if the user is a moderator
		$CanApprove = Gdn::Session()->CheckPermission('Vanilla.Categories.Manage'); // Moderators generally have this permission

		if ($CanApprove)	// if they're a moderator, render a link that will approve the comment
		{
			$Query = http_build_query(array('commentid' => GetValue('CommentID', $Comment), 'tkey' => Gdn::Session()->TransientKey()));
			echo '<div class="Pending">This post requires moderator approval. '.Anchor(T('Approve', 'Approve'), '/discussion/approve/comment?'.$Query, array('title' => T('Approve this post.'))).' it or click Delete above.</div>';
		}
		else	// otherwise, it's the author, so notify them their post is awaiting approval
		{
			echo '<div class="Pending"><b>Pending Approval:</b> This post is invisible to other users until it is approved by a moderator.</div>';
			return;
		}
      
		static $InformMessage = TRUE;

		if ($InformMessage)
		{
			$Sender->InformMessage(T('Approve or delete each pending post.'), 'Dismissable');
			$InformMessage = FALSE;
		}
	}
	
	// Render a label indicating the post requires approval and a link to approve it
	// Mostly same as above function
	public function DiscussionController_CommentInfo_Handler($Sender, $Args)
	{
		$Discussion = GetValue('Discussion', $Args);
		$Comment = GetValue('Comment', $Args);
	  
		if (!$Discussion)
			return;

		if ( !(GetValue('Hidden', $Comment) == 'Yes') && !(GetValue('Hidden', $Discussion) == 'Yes') )
			return;
		 
		$CanApprove = Gdn::Session()->CheckPermission('Vanilla.Categories.Manage');

		// Write the links.
		if (!$Comment)
		{
			if ($CanApprove)
			{
				$Query = http_build_query(array('discussionid' => GetValue('DiscussionID', $Discussion), 'tkey' => Gdn::Session()->TransientKey()));
				echo '<div class="Pending">This post requires moderator approval. '.Anchor(T('Approve', 'Approve'), '/discussion/approve/discussion?'.$Query, array('title' => T('Approve this post.'))).' it or click Delete above.</div>';
			}
			else
			{
				echo '<div class="Pending"><b>Pending Approval:</b> This post is invisible to other users until it is approved by a moderator.</div>';
				return;
			}
		}
		else
		{
			if ($CanApprove)
			{
				$Query = http_build_query(array('commentid' => GetValue('CommentID', $Comment), 'tkey' => Gdn::Session()->TransientKey()));
				echo '<div class="Pending">This post requires moderator approval. '.Anchor(T('Approve', 'Approve'), '/discussion/approve/comment?'.$Query, array('title' => T('Approve this post.'))).' it or click Delete above.</div>';
			}
			else
			{
				echo '<div class="Pending"><b>Pending Approval:</b> This post is invisible to other users until it is approved by a moderator.</div>';
				return;
			}
		}
      
		static $InformMessage = TRUE;

		if ($InformMessage)
		{
			$Sender->InformMessage(T('Approve or delete each pending post.'), 'Dismissable');
			$InformMessage = FALSE;
		}
	}
	
	// Exclude hidden posts from the comment count
	public function CommentModel_BeforeUpdateCommentCountQuery_Handler($Sender)
	{
		$Sender->SQL->Where('c.Hidden', 'No');
	}
	
	// Update the count of comments pending approval
	public function CommentModel_BeforeUpdateCommentCount_Handler($Sender)
	{
		$Discussion = $Sender->EventArguments['Discussion'];
		$DiscussionID = GetValue('DiscussionID', $Discussion);
		
		$Data = $Sender->SQL
			->Select('c.CommentID', 'count', 'CountComments')
			->From('Comment c')
			->Where('c.DiscussionID', $DiscussionID)
			->Where('c.Hidden', 'Yes')
			->Get()->FirstRow(DATASET_TYPE_ARRAY);
			
		$Sender->SQL
			->Update('Discussion')
			->Set('CountPendingComments', $Data['CountComments'])
			->Where('DiscussionID', $DiscussionID)
			->Put();
	}
   
	// Include pending comment count in discussion meta
	public function Base_AfterCountMeta_Handler($Sender)
	{
		$Discussion = $Sender->EventArguments['Discussion'];
		if (Gdn::Session()->IsValid() && $Discussion->CountPendingComments > 0 && Gdn::Session()->CheckPermission('Vanilla.Categories.Manage'))
               echo '<strong class="Pending">' . $Discussion->CountPendingComments . ' pending</strong>';

	}
	
	// Check moderator permissions when accessing a hidden discussion
	public function DiscussionController_BeforeDiscussionRender_Handler($Sender)
	{
		// will only allow moderators and the author to see hidden discussion
		if ( $Sender->Discussion->Hidden == 'Yes' && !Gdn::Session()->CheckPermission('Vanilla.Categories.Manage') &&  Gdn::Session()->UserID != $Sender->Discussion->InsertUserID)
			throw PermissionException('Vanilla.Categories.Manage');
	}
	
	// Stop a new discussion from sending notifications
	public function DiscussionModel_BeforeDiscussionNotifications_Handler($Sender)
	{
		$Discussion = $Sender->EventArguments['Discussion'];
		// Announcements bypass moderator queue
		if ( $Discussion['Announce'] == 0 && $Sender->EventArguments['UseSession'] && $Sender->EventArguments['Insert'])
			$Sender->EventArguments['Continue'] = FALSE;
	}
	
	// Stop a new comment from sending notifications
	public function CommentModel_BeforeCommentNotifications_Handler($Sender)
	{
		if ( $Sender->EventArguments['UseSession'] && $Sender->EventArguments['Insert'])
			$Sender->EventArguments['Continue'] = FALSE;
	}
   
	// After a discussion or comment is approved, handles sending notifications normally sent for new posts
	public function DiscussionController_Approve_Create($Sender, $Args)
	{
		if ($Args[0] == 'discussion')
		{
			$Discussion = Gdn::SQL()->GetWhere('Discussion', array('DiscussionID' => $Sender->Request->Get('discussionid')))->FirstRow(DATASET_TYPE_ARRAY);
			if (!$Discussion)
				throw NotFoundException('Discussion');
			
			// Check for permission.
			if (!Gdn::Session()->CheckPermission('Vanilla.Categories.Manage'))
				throw PermissionException('Vanilla.Categories.Manage');
			
			// Update the discussion.
			Gdn::SQL()->Put('Discussion', array('Hidden' => 'No'), array('DiscussionID' => $Discussion['DiscussionID']));
			
			// Record the activity
			$Discussion['IsNewDiscussion'] = TRUE;
			$Sender->DiscussionModel->SaveNotifications($Discussion, FALSE);
		   
			Redirect("/discussion/{$Discussion['DiscussionID']}");
		}
		elseif ($Args[0] == 'comment')
		{
			$Comment = Gdn::SQL()->GetWhere('Comment', array('CommentID' => $Sender->Request->Get('commentid')))->FirstRow(DATASET_TYPE_ARRAY);
			if (!$Comment)
				throw NotFoundException('Comment');

			$Discussion = Gdn::SQL()->GetWhere('Discussion', array('DiscussionID' => $Comment['DiscussionID']))->FirstRow(DATASET_TYPE_ARRAY);

			// Check for permission.
			if (!Gdn::Session()->CheckPermission('Vanilla.Categories.Manage'))
				throw PermissionException('Vanilla.Categories.Manage');
			
			// Update the comment.
			Gdn::SQL()->Put('Comment', array('Hidden' => 'No'), array('CommentID' => $Comment['CommentID']));

			// Record the activity
			$CommentID = GetValue('CommentID', $Comment);
			$Sender->CommentModel->Save2($CommentID, TRUE, TRUE, TRUE, FALSE);
				
			//Update discussion comment count
			$DiscussionID = GetValue('DiscussionID', $Discussion);
			$Sender->CommentModel->UpdateCommentCount($DiscussionID);
			
			Redirect("/discussion/comment/{$Comment['CommentID']}#Comment_{$Comment['CommentID']}");
        }
		else
			Redirect("/discussions/");
	}
	
	// Send email notifications to moderators who have elected to be notified when a new post requires approval
	public function PostController_AfterCommentSave_Handler($Sender)
	{
		$IsModerator = Gdn::Session()->CheckPermission('Vanilla.Categories.Manage');
		if ( !$Sender->EventArguments['Editing'] && !$IsModerator ) // ignores editing and moderator posts
		{
		
		$SQL = Gdn::SQL();
			
		$SqlUsers = $SQL->Select('m.UserID, u.Name, u.Email')
			->From('UserMeta m')
			->Join('User u', 'm.UserID = u.UserID')
			->Where('u.Deleted', '0')
			->Where('m.Name', 'Preferences.Email.PostApproval') 
			->Where('m.Value', '1') 
			->Get();
			  
		$Users = array();
		while($User = $SqlUsers->NextRow(DATASET_TYPE_ARRAY))
		{
			$Users[] = array( $User['UserID'], $User['Name'], $User['Email']);
		}
		
		$Comment = $Sender->EventArguments['Comment'];
		$Discussion = $Sender->EventArguments['Discussion'];
		$CommentID = GetValue('CommentID', $Comment);
		
		$Query = http_build_query(array('commentid' => GetValue('CommentID', $Comment)));
		$url = Url("/discussion/approve/comment&{$Query}", TRUE);
		
		$InsertUser = $SQL->Select('Name')
		       ->From('User')
		       ->Where('UserID', GetValue('InsertUserID', $Comment)) 
		       ->Get()->FirstRow(DATASET_TYPE_ARRAY);
		
		$Subject = 'New comment requires approval';
		$ActivitySubHead = $InsertUser['Name'] . ' posted a new comment that requires approval.';
		$Story = "[RE: " . GetValue('Name', $Discussion) . "]\n" . Gdn_Format::Text(GetValue('Body', $Comment));
		$Story .= "\n\nClick this link to APPROVE the comment:\n" . $url;

		foreach ($Users as $User) {
			$Email = new Gdn_Email();
			$Email->Subject(sprintf(T('[%1$s] %2$s'), Gdn::Config('Garden.Title'), $Subject));
			$Email->To($User[2], $User[1]);
			//$Email->From(Gdn::Config('Garden.SupportEmail'), Gdn::Config('Garden.SupportName'));
			$Email->Message(
				sprintf(
				T('EmailStoryNotification'),
				$ActivitySubHead,
				Url("/discussion/comment/{$CommentID}#Comment_{$CommentID}", TRUE),
				$Story
				)
			);

			try
			{
				$Email->Send();
			}
			catch (Exception $ex)
			{
				echo $ex;
				die();
				// Don't do anything with the exception.
			}
		}
	
		$Sender->StatusMessage = T('Your post will appear once it has been approved.');
		
		}
		else if ($IsModerator)
		{
			$Comment = $Sender->EventArguments['Comment'];
			$Discussion = $Sender->EventArguments['Discussion'];
			$CommentID = GetValue('CommentID', $Comment);
			Gdn::SQL()->Put('Comment', array('Hidden' => 'No'), array('CommentID' => $CommentID));
			
			// Record the activity
			$CommentID = GetValue('CommentID', $Comment);
			$Sender->CommentModel->Save2($CommentID, TRUE, TRUE, TRUE, FALSE);
				
			//Update discussion comment count
			$DiscussionID = GetValue('DiscussionID', $Discussion);
			$Sender->CommentModel->UpdateCommentCount($DiscussionID);
		}
	}
	
	// Ensures the comment count is not decremented when a hidden post is removed, as it was never incremented
	public function CommentModel_BeforeDeleteComment_Handler($Sender)
	{
		$CommentID = $Sender->EventArguments['CommentID'];
		
		$Result = $Sender->SQL
			->Select('Hidden')
			->From('Comment')
			->Where('CommentID', $CommentID)
			->Get()->FirstRow(DATASET_TYPE_ARRAY);
			
		if ( $Result['Hidden'] == 'Yes' )
		{
			$Sender->EventArguments['UpdateCount'] = FALSE;
			$Sender->SQL
				->Update('Discussion')
				->Set('CountPendingComments', 'CountPendingComments - 1', FALSE)
				->Where('DiscussionID', $Sender->EventArguments['DiscussionID'])
				->Put();
		}
	}
	
	public function Base_BeforeDiscussionSql_Handler($Sender)
	{
		$params = array('No');
		$Sender->SQL->Select('d.Hidden as Hidden')
		->WhereIn('d.Hidden', $params);
	}
	
	public function Base_BeforeCommentSql_Handler($Sender)
	{
		$params = array('No');
		$Sender->SQL->Select('c.Hidden as Hidden')
		->WhereIn('c.Hidden', $params);
	}
}

?>