<?php if (!defined('APPLICATION')) exit();

$PluginInfo['UserCustomFields'] = array(
    'Name'                 => 'User Custom Fields',
    'Description'          => 'User custom fields for Kazakhstan Ministry of Regional Development Forum.',
    'Version'              => '1.0',
    'RequiredPlugins'      => FALSE,
    'RegisterPermissions'  => FALSE,
    'SettingsUrl'          => FALSE,
    'SettingsPermission'   => FALSE,
    'RequiredApplications' => array('Vanilla' => '>=2.0.18'),
    'Author'               => 'Juan Obando',
    'AuthorEmail'          => 'juanchopx2@gmail.com',
    'AuthorUrl'            => 'http://about.me/juanchopx2'
);

/**
 *
 */
class UserCustomFieldsPlugin extends Gdn_Plugin {
    /**
     * Plugin setup
     */
    public function Setup() {
        Gdn::Structure()
            ->Table('User')
            ->Column('PhoneNumber', 'varchar(24)', FALSE)
            ->Column('CompanyName', 'varchar(32)', TRUE)
            ->Column('JobTitle', 'varchar(32)', TRUE)
            ->Set(FALSE, FALSE);
    }

    /**
     * Plugin cleanup
     */
    public function OnDisable() {
        Gdn::Structure()->Table('User')->DropColumn('PhoneNumber');
        Gdn::Structure()->Table('User')->DropColumn('CompanyName');
        Gdn::Structure()->Table('User')->DropColumn('JobTitle');
    }

    /**
     * Custom template for register aproval method
     */
    public function EntryController_Render_Before($Sender) {
        $this->PrepareController($Sender);

        if ($Sender->RequestMethod === 'register') {
            if (($Sender->View !== 'registerthanks') && ($Sender->View !== 'registerclosed')) {
                $Sender->View = $this->GetView('entry/registerapproval.php');
            }
        }
    }

    /**
     *
     */
    public function UserController_Render_Before($Sender) {
        $this->PrepareController($Sender);

        if ($Sender->RequestMethod === 'index') {
            // $Sender->View = $this->GetView('user/index.php');
        } elseif ($Sender->RequestMethod === 'add') {
            $Sender->View = $this->GetView('user/add.php');
        } elseif ($Sender->RequestMethod === 'edit') {
            $Sender->View = $this->GetView('user/edit.php');
        }
    }

    /**
     *
     */
    protected function PrepareController($Sender) {
        $Sender->AddJsFile('usercustomfields.js', 'plugins/UserCustomFields');
    }
}
