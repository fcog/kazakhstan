<?php

$Definition['Activity.AnswerAccepted.FullHeadline'] = '%1$s accepted %4$s %8$s.';
$Definition['Activity.QuestionAnswer.FullHeadline'] = '%1$s answered %4$s %8$s.';

$Definition['Badge404'] = 'Badge not found.';
$Definition['BadgesModuleTitle'] = 'Badges';
$Definition['BadgesNobody'] = 'Nobody has earned this badge yet.';

$Definition['Commenting as %1$s (%2$s)'] = 'Commenting as %1$s <span class="SignOutWrap">(%2$s)</span>';

$Definition['Date.DefaultDateTimeFormat'] = '%B %e, %Y %l:%M%p';
$Definition['Define who can upload files on the Roles & Permissions page.'] = 'Define who can upload and manage files on the <a href="%s">Roles & Permissions</a> page.';
$Definition['Draft.Delete'] = '×';

$Definition['Email Source'] = 'Email';
$Definition['EmbeddedDiscussionFormat'] = '<div class="EmbeddedContent">{Image}<strong>{Title}</strong>
<p>{Excerpt}</p>
<p><a href="{Url}">Read the full story here</a></p><div class="ClearFix"></div></div>';
$Definition['EmbeddedNoBodyFormat'] = '{Url}';
$Definition['EmbededDiscussionFormat'] = '<div class="EmbeddedContent">{Image}<strong>{Title}</strong>
<p>{Excerpt}</p>
<p><a href="{Url}">Read the full story here</a></p><div class="ClearFix"></div></div>';

$Definition['Garden.Import.Complete.Description'] = 'You have successfully completed an import.
   Click <b>Finished</b> when you are ready.';
$Definition['Garden.Import.Continue.Description'] = 'It appears as though you are in the middle of an import.
   Please choose one of the following options.';
$Definition['Garden.Import.Merge.Description'] = 'This will merge all of the user and discussion data from the import into this forum.
<b>Warning: If you merge the same data twice you will get duplicate discussions.</b>';

$Definition['HeadlineFormat.Badge'] = '{ActivityUserID,You} earned the <a href="{Url,html}">{Data.Name,text}</a> badge.';
$Definition['HeadlineFormat.Ban'] = '{RegardingUserID,You} banned {ActivityUserID,you}.';
$Definition['HeadlineFormat.Comment'] = '{ActivityUserID,user} commented on <a href="{Url,html}">{Data.Name,text}</a>';
$Definition['HeadlineFormat.Discussion'] = '{ActivityUserID,user} Started a new discussion. <a href="{Url,html}">{Data.Name,text}</a>';
$Definition['HeadlineFormat.Mention'] = '{ActivityUserID,user} mentioned you in <a href="{Url,html}">{Data.Name,text}</a>';
$Definition['HeadlineFormat.PictureChange.ForUser'] = '{RegardingUserID,You} changed the profile picture for {ActivityUserID,user}.';
$Definition['HeadlineFormat.Registration'] = '{ActivityUserID,You} joined.';
$Definition['HeadlineFormat.Unban'] = '{RegardingUserID,You} unbanned {ActivityUserID,you}.';
$Definition['HeadlineFormat.Warning'] = '{ActivityUserID,You} warned {RegardingUserID,you}.';

$Definition['Invalid password.'] = 'The password you entered was incorrect. Remember that passwords are case-sensitive.';

$Definition['Marking as spam cannot be undone.'] = 'Marking something as SPAM will cause it to be deleted forever. Deleting is a good way to keep your forum clean.';
$Definition['MoneyFormat2'] = '$%7.2f';
$Definition['MyBadgesModuleTitle'] = 'My Badges';

$Definition['NoBadgesEarned'] = 'Any minute now&hellip;';
$Definition['Null Date'] = '-';

$Definition['Operation By'] = 'By';

$Definition['ParticipatedHomepageTitle'] = 'Participated Discussions';
$Definition['PermissionRequired.Garden.Moderation.Manage'] = 'You need to be a moderator to do that.';
$Definition['PermissionRequired.Garden.Settings.Manage'] = 'You need to be an administrator to do that.';
$Definition['PermissionRequired.Javascript'] = 'You need to enable javascript to do that.';
$Definition['ProfileFieldsCustomDescription'] = 'Use these fields to create custom profile information. You can enter things like "Relationship Status", "Skype", or "Favorite Dinosaur". Be creative!';
$Definition['ProxyConnect.RimBlurb'] = 'If you are using ProxyConnect with an officially supported remote application plugin such as our wordpress-proxyconnect plugin, these values will be available in that plugin\'s configuration screen.';

$Definition['Q&A Accepted'] = 'Answered ✓';
$Definition['Q&A Answered'] = 'Answered';
$Definition['QnA Accepted Answer'] = 'Answer ✓';
$Definition['QnA Rejected Answer'] = 'Rejected Answer';
$Definition['Quote on'] = 'on';
$Definition['Quote wrote'] = 'wrote';

$Definition['Ranks.ActivityFormat'] = '{ActivityUserID,user} {ActivityUserID,plural,was,were} promoted to {Data.Name,plaintext}.';
$Definition['Ranks.NotificationFormat'] = 'Congratulations! You\'ve been promoted to {Data.Name,plaintext}.';

$Definition['Search for a tag.'] = 'Search for all or part of a tag.';
$Definition['Sign In or Register to Comment.'] = '<a href="{SignInUrl,html}"{Popup}>Sign In</a> or <a href="{RegisterUrl,html}">Register</a> to comment.';
$Definition['SigningOut'] = 'Hang on a sec while we sign you out.';

$Definition['Test Mode'] = 'Test Mode: The pocket will only be displayed for pocket administrators.';
$Definition['The quote had to be converted from %s to %s.'] = 'The quote had to be converted from %s to %s. Some formatting may have been lost.';

$Definition['User not found.'] = 'Sorry, no account could be found related to the email/username you entered.';

$Definition['ValidateFormat'] = 'You are not allowed to post raw html.';
$Definition['ValidateIntegerArray'] = '%s must be a comma-delimited list of numbers.';
$Definition['ValidateTag'] = 'Tags cannot contain spaces.';
$Definition['ValidateUrlStringRelaxed'] = '%s can not contain slashes, quotes or tag characters.';

$Definition['WarningTitleFormat'] = '{InsertUserID,User} warned {WarnUserID,User} for {Points,plural,%s points}.';
$Definition['WarningTitleFormat.Notice'] = '{InsertUserID,User} warned {WarnUserID,User} for {Points,plural,%s points} (just a notice).';
$Definition['Wordpress Source'] = 'Wordpress';

$Definition['You can use HTML in your signature.'] = 'You can use <b><a href="http://htmlguide.drgrog.com/cheatsheet.php" target="_new">Simple Html</a></b> in your signature.';
$Definition['You were added to a conversation.'] = '{InsertUserID,user} added {NotifyUserID,you} to a <a href="{Url,htmlencode}">conversation</a>.';
$Definition['YouEarnedBadge'] = 'You earned this badge';
