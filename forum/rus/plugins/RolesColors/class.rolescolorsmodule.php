<?php if (!defined('APPLICATION')) exit();

/**
 *
 */
class RolesColorsModule extends Gdn_Module {
    protected $_UsersRoles;

    /**
     *
     */
    public function __construct($Sender = '') {
        $_UsersRoles = FALSE;
        parent::__construct($Sender);
    }

    /**
     *
     */
    public function AssetTarget() {
        return 'Panel';
    }

    /**
     *
     */
    public function GetUserDiscussionRoles() {
        $SQL = Gdn::SQL();
        $this->_UsersRoles = $SQL->Select('RoleID, Name')
            ->From('Role')
            ->WhereIn('RoleID', array(33, 34, 35, 36, 37, 38, 39))
            ->Get();
    }

    /**
     *
     */
    public function ToString() {
        if ($this->_UsersRoles->NumRows() == 0) {
            return '';
        }

        $String = '';
        ob_start();
        ?>
        <div class="Box">
            <h4><?php echo T('Roles Colors'); ?></h4>
            <ul class="PanelInfo">
                <?php foreach ($this->_UsersRoles->Result() as $UserRole) : ?>
                <li>
                    <span>
                        <strong class="RoleColorRow"><?php echo $UserRole->Name; ?></strong>
                        <span class="<?php echo 'Role_' . $UserRole->RoleID; ?> Count">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    </span>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <?php
        $String = ob_get_contents();
        @ob_end_clean();
        return $String;
    }
}