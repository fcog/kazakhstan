<?php if (!defined('APPLICATION')) exit();

$PluginInfo['RolesColors'] = array(
    'Name'                 => 'Roles Colors',
    'Description'          => 'Set colors for each role in the forum.',
    'Version'              => '1.0',
    'RequiredPlugins'      => FALSE,
    'RegisterPermissions'  => FALSE,
    'SettingsUrl'          => FALSE,
    'SettingsPermission'   => FALSE,
    'RequiredApplications' => array('Vanilla' => '>=2.0.18'),
    'Author'               => 'Juan Obando',
    'AuthorEmail'          => 'juanchopx2@gmail.com',
    'AuthorUrl'            => 'http://about.me/juanchopx2'
);

/**
 *
 */
class RolesColorsPlugin extends Gdn_Plugin {
    /**
     * Plugin setup
     */
    public function Setup() {}

    /**
     *
     */
    public function DiscussionsController_Render_Before(&$Sender) {
        $this->Render_Roles_Palette($Sender);
    }

    /**
     *
     */
    public function DiscussionController_BeforeDiscussionRender_Handler(&$Sender) {
        $this->Render_Roles_Palette($Sender);
    }

    /**
     *
     */
    protected function Render_Roles_Palette($Sender) {
        require_once(PATH_PLUGINS . DS . 'RolesColors' . DS . 'class.rolescolorsmodule.php');
        $RolesColorsModule = new RolesColorsModule($Sender);
        $RolesColorsModule->GetUserDiscussionRoles();
        $Sender->AddCssFile('plugins/RolesColors/design/style.css');
        $Sender->AddModule($RolesColorsModule);
    }
}