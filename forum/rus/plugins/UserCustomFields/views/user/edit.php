<?php if (!defined('APPLICATION')) exit(); ?>
<h1><?php
   if (is_object($this->User))
      echo T('Edit User');
   else
      echo T('Add User');
?></h1>
<?php
echo $this->Form->Open(array('class' => 'User'));
echo $this->Form->Errors();
if ($this->Data('AllowEditing')) { ?>
   <ul>
      <li>
         <?php
            echo $this->Form->Label('Username', 'Name');
            echo $this->Form->TextBox('Name');
         ?>
      </li>
      <li>
         <?php
            echo $this->Form->Label('Email', 'Email');
            echo $this->Form->TextBox('Email');
         ?>
      </li>
      <li>
         <?php
            echo $this->Form->CheckBox('ShowEmail', T('Email visible to other users'), array('value' => '1'));
         ?>
      </li>
      <li>
         <?php
            echo $this->Form->CheckBox('Banned', T('Banned'), array('value' => '1'));
         ?>
      </li>
      <li>
         <?php
            echo $this->Form->Label('Phone Number', 'PhoneNumber');
            echo $this->Form->TextBox('PhoneNumber');
         ?>
      </li>
   </ul>
   <ul>
      <li>
         <?php
            $rolesIDs = array_keys($this->UserRoleData);
            echo $this->Form->Label('Role', 'RoleID');
            echo $this->Form->DropDown('RoleID', $this->RoleData, array('name' => 'RoleID[]', 'value' => $rolesIDs[0]));
         ?>
      </li>
   </ul>
   <ul class="JobInfo">
      <li>
         <?php
            echo $this->Form->Label('Company Name', 'CompanyName');
            echo $this->Form->TextBox('CompanyName');
         ?>
      </li>
      <li>
         <?php
            echo $this->Form->Label('Job Title', 'JobTitle');
            echo $this->Form->TextBox('JobTitle');
         ?>
      </li>
   </ul>
   <h3><?php echo T('Password Options'); ?></h3>
   <ul>
      <li>
         <?php
            echo $this->Form->Label('Reset Password');
            echo $this->Form->CheckBox('ResetPassword', T('Reset password and send email notification to user'));
         ?>
      </li>
      <li id="NewPassword">
         <?php
            echo $this->Form->Label('New Password', 'NewPassword');
            echo $this->Form->Input('NewPassword', 'password');
         ?>
         <div class="InputButtons">
            <?php
               echo Anchor(T('Generate Password'), '#', 'GeneratePassword Button SmallButton');
               echo Anchor(T('Reveal Password'), '#', 'RevealPassword Button SmallButton');
            ?>
         </div>
      </li>
   </ul>
<?php

   $this->FireEvent('AfterFormInputs');
   echo $this->Form->Close('Save');
}