jQuery(function($) {
    console.log('testing');
    $('#Form_Form').find('.FilterMenu').find('.SmallButton').click(function() {
        waitForUserFormWindow();
    });

    $('#Users').find('.Popup').click(function() {
        waitForUserFormWindow();
    });

    $('#Form_RoleID').live('change', function() {
        evalSelectedRole($('#Form_RoleID').val());
    });
});

var waitForUserFormWindow = function() {
    setTimeout(function() {
        evalSelectedRole($('#Form_RoleID').val());
    }, 500);
}

var evalSelectedRole = function(roleId) {
    if ((roleId === '33') || (roleId === '34')) {
        $('.JobInfo').show();
    } else {
        $('.JobInfo').hide();
    }
}