<?php
/* Include here the additional functions
 * Please note that functions defined on the parent child ARE included.
 */

load_child_theme_textdomain( 'qproject', get_stylesheet_directory() . '/languages' );

include( 'functions/scripts.php' );
include( 'functions/custom-wp-functions.php' );
include( 'functions/sidebars.php' );
include( 'functions/home-page-featured-articles.php' );
include( 'functions/custom.php' );
include( 'functions/home-page-map.php' );