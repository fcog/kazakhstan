<?php
/**
 * Template for the footer of the website. Closes all tags opened in the header.php file
 *
 * @package qProject
 * @see header.php
 */
?>
      </div>

      <footer>
        <div class="footer-menu grid_25">
          <?php wp_nav_menu( array(
            'theme_location' => 'footer',
            //'menu'           => 6,
            'container'      => 'div'
          ) ); ?>
        </div>

        <div class="clear margin-bottom-24"></div>

        <div class="brand-link grid_24">
          <div class="grid_12 alpha">
          <?php echo __('&copy; Copyright').' - '. __('Ministry of Regional Development of the Republic of Kazakhstan', 'qproject'); ?>
          </div>

          <div class="grid_7 push_5 omega">
            <a href="http://orphus.ru" id="orphus" target="_blank" title="System Orphus"><img alt="System Orphus" src="<?php echo get_stylesheet_directory_uri() ?>/images/orphus-<?php global $blog_id;
                if ($blog_id == 1) { // Kazakh
                    echo "kaz";
                } else if ($blog_id == 2) { // Russian
                    echo "ru";
                } else { // English
                    echo "en";
                } ?>.gif" border="0" title="System Orphus"></a>
          </div>
        </div>
      </footer>

    </div>

    <?php wp_footer(); ?>
  </body>
</html>