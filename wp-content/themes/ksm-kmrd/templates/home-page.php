<?php
/**
 * @package WordPress
 * @subpackage argos
 * Template Name: Home Page
 */
/**
 * NOTE: Since it is a MS environment, the ID of the home page changes
 */
global $post;
$home_post_ID = $post->ID;

get_header();
?>

<div id="qproject-home-content" class="grid_24">

    <div class="grid_8 alpha">
		<?php get_template_part('templates/featured', 'articles'); ?>
    </div>

	<?php
	// check the category ID, based on the blog ID
	if ($blog_id == 1) { // Kazakh
		$post_id = 196;
	} else if ($blog_id == 2) { // Russian
		$post_id = 196;
	} else { // English
		$post_id = 196;
	}

	$kaz_content = get_post($post_id);
	?>

	<div class="grid_16 omega">
        <div id="map-zones">
			<div id="map" class="map"></div>
            <div id="city1"></div>
            <div id="city2"></div>
			<div id="text">
				<h2><span><?php echo $kaz_content->post_title; ?></span></h2>
				<div class="region-desc"><?php echo $kaz_content->post_content; ?></div>
			</div>
        </div>
    </div>


    <div class="clear margin-bottom-12"></div>

    <div class="grid_12 alpha extend">
        <div id="home-page-left">
            <div class="latest-news">
				<h2>
                    <?php echo __( 'News', 'qproject' ); ?>
                    <a class="view-all" href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>"><?php echo __( 'View all news &raquo;', 'qproject' ); ?></a>
                </h2>
				<?php
                global $blog_id;
                // Do not display featured articles - get featured articles category id
                if ($blog_id == 1) { // Kazakh
                    $category = -11;
                } else if ($blog_id == 2) { // Russian
                    $category = -7;
                } else { // English
                    $category = -7;
                }

				// $posts = get_posts(array(
    //                         //'category' => $category,
    //                         'post_type' => 'post',               
    //     					'numberposts' => 3,
    //     					'orderby' => 'post_date',
    //     					'order' => 'DESC',
    //     				));

                $exclude_articles = "";

                foreach ($articles as $key => $value) {
                    $exclude_articles .= "$wpdb->posts.ID <> ".$value." AND ";
                }

                $str = "SELECT * FROM $wpdb->posts 
                        INNER JOIN $wpdb->term_relationships ON($wpdb->posts.ID = $wpdb->term_relationships.object_id)  
                        INNER JOIN $wpdb->term_taxonomy ON($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)  

                        WHERE (".$exclude_articles."
                            $wpdb->term_taxonomy.term_id <> 7  
                           AND $wpdb->term_taxonomy.taxonomy = 'category'   
                           AND $wpdb->posts.post_type = 'post'  
                           AND $wpdb->posts.post_status = 'publish') order by $wpdb->posts.post_date DESC limit 3";

                //echo($str);

                $posts = $wpdb->get_results($str);
				?>

                <?php $i = 1; ?>

                <?php foreach ($posts as $post) : setup_postdata($post); ?>
					<div class="news">
						<div class="news-date news-order-<?php echo $i; ?>">
	                       <?php echo get_the_date('M'); ?>
					       <span><?php echo get_the_date('j'); ?></span>
						</div>
						<div class="news-content">
							<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
							<!-- p -->
                            <?php the_excerpt(); // echo inq_excerpt(get_the_content(), 100, "..."); ?>
							<!-- /p -->
							<a href="<?php the_permalink(); ?>" class="read-more"><?php echo __( 'Read more &raquo;', 'qproject' ); ?></a>
						</div>
					</div>
                    <?php $i++; ?>
                <?php endforeach; ?>
            </div>

            <div class="statistics">
                <h2><?php echo __( 'Infographics', 'qproject' ); ?></h2>
                <div class="content-statistics">
                    <!-- <img src="<?php echo get_post_meta( $home_post_ID, 'homepage_stats_widget', true ); ?>" /> -->
                    <?php
                        if (is_active_sidebar('sidebar-home-infographics')) :
                            dynamic_sidebar('sidebar-home-infographics');
                        endif;
                    ?>
                </div>
            </div>
        </div>
    </div>


    <div class="grid_12 omega extend">
        <div id="home-page-right-top" class="margin-bottom-12">
            <ul>
				<?php
				if (is_active_sidebar('sidebar-home-rigth')) :
					dynamic_sidebar('sidebar-home-rigth');
				endif;
				?>
            </ul>
        </div>

        <div class="home-page-right-bottom top margin-bottom-12">
            <h2><?php echo __( 'Featured Question', 'qproject' ); ?></h2>
            <div class="featured-question-banner">
                <?php
                if (is_active_sidebar('sidebar-home-featured-question-banner')) :
                    dynamic_sidebar('sidebar-home-featured-question-banner');
                endif;
                ?>                
            </div>
            <div class="featured-question-content">
                <?php $last_discussion = get_last_discussion(); ?>
                <?php if ( !empty( $last_discussion ) ) : ?>      
                    <?php $last_comment = get_last_discussion_comment( $last_discussion->DiscussionID ) ?>
                    <div>
                        <h3>
                            <?php if (get_bloginfo('language') == 'ru-RU'): ?>
                                <a href="<?php echo get_bloginfo('url') . '/обсуждения/#/discussion/' . $last_discussion->DiscussionID; ?>"><?php echo $last_discussion->Name; ?></a>
                            <?php else: ?>
                                <a href="<?php echo get_bloginfo('url') . '/discussions/#/discussion/' . $last_discussion->DiscussionID; ?>"><?php echo $last_discussion->Name; ?></a>
                            <?php endif ?>
                        </h3>
                        <p><?php echo $last_comment; ?></p>
                    </div>
                <?php else : ?>
                    <div>
                       <?php if (get_bloginfo('language') == 'ru-RU'): ?>
                            <a href="<?php echo get_bloginfo('url') . '/обсуждения/#/discussions' ?>"><?php echo __( 'No discussions in the forum.', 'qproject' ) ?></a>
                        <?php else: ?>
                            <a href="<?php echo get_bloginfo('url') . '/discussions/#/discussions' ?>"><?php echo __( 'No discussions in the forum.', 'qproject' ) ?></a>
                        <?php endif ?>                        
                    </div>
                <?php endif; ?>
            </div>
            <!-- <div class="featured-question-total"> -->
            <?php // echo $discussions->CountDiscussions;  ?>
            <!-- </div> -->
        </div>

        <div class="home-page-right-bottom">
            <ul>
                <?php
                if (is_active_sidebar('sidebar-home-youtube')) :
                    dynamic_sidebar('sidebar-home-youtube');
                endif;
                ?>
            </ul>
        </div>       

        <div class="home-page-right-bottom">
            <ul>
                <?php
                if (is_active_sidebar('sidebar-home-flickr')) :
                    dynamic_sidebar('sidebar-home-flickr');
                endif;
                ?>
            </ul>
        </div>            

    </div>

    <div class="clear margin-bottom-24"></div>

	<?php
	if (is_active_sidebar('twitter-area')) :
		dynamic_sidebar('twitter-area');
	endif;
	?>

	<?php if (is_active_sidebar('sidebar-home-banners')) : ?>
	<div id="homepage-banners" class="grid_24 alpha omega">
        <a class="nav-left" href="#">&nbsp;</a>
        <a class="nav-right" href="#">&nbsp;</a>
		<ul class="banners-list">
		  <?php dynamic_sidebar('sidebar-home-banners'); ?>
		</ul>
	</div>
	<?php endif; ?>

</div>

<div class="clear margin-bottom-24"></div>

<?php get_footer(); ?>