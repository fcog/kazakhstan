<?php
    /**
     * This template show the last nine (9) posts under the category called "featured articles"
     * It's very important that the files: jquery.cycle.all.js and home-page-featured-articles.js
     * can be loaded in the template that contains this code.
     *
     * @jucachap
     */
	 global $blog_id;
?>

<div id="home-top-left-container">

    <?php
		// Get the Featured Articles Category id
		if ($blog_id == 1) { // Kazakh
			$category = 11;
		} else if ($blog_id == 2) { // Russian
			$category = 7;
		} else { // English
			$category = 7;
		}

        $args = array('category'=>$category, 'numberposts'=>9 );
        $featured_posts = get_posts($args);
        global $wpdb;

        global $articles;

        $articles = array();

        if (count($featured_posts)):
    ?>

    <div id="featured-articles">
    <?php foreach ($featured_posts as $article) :
      $articles[] = $article->ID;
      $img = wp_get_attachment_image_src( get_post_thumbnail_id($article->ID), 'homepage-thumb' );
    ?>
        <div id="fa-slide-<?php echo ($article->ID); ?>" class="item">
            <div class="fa-slide-images">
            <?php
            if ($img) { ?>
                <a href="<?php echo get_permalink($article->ID); ?>" title="<?php $article->post_title; ?>">
                    <img src="<?php echo $img[0] ?>" alt="<?php $article->post_title; ?>" />
                </a>
                <?php
            }
            else {
            ?>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/featured-articles-default.png" heigth="190" width="308" alt="featured image article" title="featured image article" />
            <?php
            }
            ?>
            </div>
            <div class="info">
                <h2><a href="<?php echo get_permalink( $article->ID ); ?>"><?php echo $article->post_title; ?></a></h2>
                <?php if( false ): ?>
                <p>
                <?php
                    $timestamp = strtotime($article->post_date);
                    echo date('F j, Y', $timestamp);
                ?>
                </p>
                <?php endif; ?>
                <p>
                <?php
                    // if(!$article->post_excerpt) {
                    //     echo inq_excerpt($article->post_content, 110, "...");
                    // }
                    // else{
                    //     echo $article->post_excerpt;
                    // }
                ?>
                    <?php echo $article->post_excerpt; ?>
                    <a href="<?php echo get_permalink( $article->ID ); ?>"><?php echo __( 'Read more &raquo;', 'qproject' ) ?></a>
                </p>
            </div><!-- #info -->
        </div>
        <?php endforeach; ?>
    </div><!-- #featured-articles -->

    <div id="nav">
        <a href="#"><span id="prev"><?php echo __( 'Prev', 'qproject' ) ?></span></a>
        <ul id="fa-slide-nav"></ul>
        <a href="#"><span id="next"><?php echo __( 'Next', 'qproject' ) ?></span></a>
    </div><!-- #nav -->
    <?php
        endif;
    ?>

</div>