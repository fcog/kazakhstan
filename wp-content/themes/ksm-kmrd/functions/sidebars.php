<?php

if ( !function_exists( 'kmrd_register_sidebars' ) ) {
  function kmrd_register_sidebars() {
    unregister_sidebar('sidebar-home-rigth');
    register_sidebar( array(
      'id'            => 'sidebar-home-rigth',
      'name'          => __( 'Home - Rigth Sidebar', 'qproject' ),
      'before_title'  => '<h2 class="">',
      'after_title'   => '</h2>',
    ) );

    unregister_sidebar('sidebar-home-infographics');
    register_sidebar( array(
      'id'            => 'sidebar-home-infographics',
      'name'          => __( 'Home - Infographics Sidebar', 'qproject' ),
      'before_title'  => '<h2 class="">',
      'after_title'   => '</h2>',
    ) );  

    unregister_sidebar('sidebar-home-featured-question-banner');
    register_sidebar( array(
      'id'            => 'sidebar-home-featured-question-banner',
      'name'          => __( 'Home - Featured Question Banner Sidebar', 'qproject' ),
      'before_title'  => '<h2 class="">',
      'after_title'   => '</h2>',
    ) );         

    unregister_sidebar('sidebar-home-youtube');
    register_sidebar( array(
      'id'            => 'sidebar-home-youtube',
      'name'          => __( 'Home - Youtube Sidebar', 'qproject' ),
      'before_title'  => '<h2 class="">',
      'after_title'   => '</h2>',
    ) );

    unregister_sidebar('sidebar-home-flickr');
    register_sidebar( array(
      'id'            => 'sidebar-home-flickr',
      'name'          => __( 'Home - Flickr Sidebar', 'qproject' ),
      'before_title'  => '<h2 class="">',
      'after_title'   => '</h2>',
    ) );

    unregister_sidebar('sidebar-home-banners');
    register_sidebar( array(
      'id'            => 'sidebar-home-banners',
      'name'          => __( 'Home - Banners Sidebar', 'qproject' ),
      'before_title'  => '<h2 class="">',
      'after_title'   => '</h2>',
    ) );

    unregister_sidebar('twitter-area');
    register_sidebar( array(
      'id'            => 'twitter-area',
      'name'          => __( 'Twitter Area', 'qproject' ),
      'before_widget' => '<div class="twitter-feeds grid_24 alpha omega">',
      'after_widget'  => "</div>",
      'before_title'  => '<h2 class="">',
      'after_title'   => '</h2>',
    ) );

    unregister_sidebar('sidebar-header-right');
    register_sidebar( array(
      'id'            => 'sidebar-header-right',
      'name'          => __( 'Home - Header Login Sidebar', 'qproject' ),
      'before_widget' => '<div class="widget-header-right">',
      'after_widget'  => "</div>",
      'before_title'  => '<h2 class="">',
      'after_title'   => '</h2>',
    ) );

		unregister_sidebar('sidebar-header-follow');
    register_sidebar( array(
      'id'            => 'sidebar-header-follow',
      'name'          => __( 'Home - Header Follow Sidebar', 'qproject' ),
      'before_widget' => '<div class="widget-header-follow">',
      'after_widget'  => "</div>",
      'before_title'  => '<h2 class="">',
      'after_title'   => '</h2>',
    ) );

    unregister_sidebar('sidebar-header-search');
    register_sidebar( array(
      'id'            => 'sidebar-header-search',
      'name'          => __( 'Home - Header Search Sidebar', 'qproject' ),
      'before_widget' => '<div class="widget-header-search">',
      'after_widget'  => "</div>",
      'before_title'  => '<h2 class="">',
      'after_title'   => '</h2>',
    ) );

    unregister_sidebar('sidebar-right-press-release');
    register_sidebar( array(
      'id'            => 'sidebar-right-press-release',
      'name'          => __( 'Right Sidebar - Press Release', 'qproject' ),
      'before_title'  => '<h2 class="">',
      'after_title'   => '</h2>',
    ) );

    unregister_sidebar('sidebar-right-publications-media');
    register_sidebar( array(
      'id'            => 'sidebar-right-publications-media',
      'name'          => __( 'Right Sidebar - Publications in the media', 'qproject' ),
      'before_title'  => '<h2 class="">',
      'after_title'   => '</h2>',
    ) );  

    unregister_sidebar('sidebar-right-publications-media');
    register_sidebar( array(
      'id'            => 'sidebar-right-publications-media',
      'name'          => __( 'Right Sidebar - Publications in the media', 'qproject' ),
      'before_title'  => '<h2 class="">',
      'after_title'   => '</h2>',
    ) );   

    unregister_sidebar('sidebar-right-media-reports');
    register_sidebar( array(
      'id'            => 'sidebar-right-media-reports',
      'name'          => __( 'Right Sidebar - Media Reports', 'qproject' ),
      'before_title'  => '<h2 class="">',
      'after_title'   => '</h2>',
    ) );

    unregister_sidebar('sidebar-right-speeches');
    register_sidebar( array(
      'id'            => 'sidebar-right-speeches',
      'name'          => __( 'Right Sidebar - Speeches', 'qproject' ),
      'before_title'  => '<h2 class="">',
      'after_title'   => '</h2>',
    ) );                 

  }
}

add_action( 'init', 'kmrd_register_sidebars', 10 );