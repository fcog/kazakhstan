<?php

//Funtion to get content from a custom post type: Map
function get_content_map( $ID ) {
    $content = '';
    $coat_arms = '';
    $custom_fields = array( 'region_akim', 'region_area', 'region_population', 'region_timezone' );
    $counter = 0;

    // get the post info
    $region_post = get_post( $ID );

    // foreach( $custom_fields as $custom_field_name ) {
    //     $custom_field = get_field_object( $custom_field_name, $ID );

    //     if ( $custom_field['value'] ) {
    //         $content .= '<div class="region_att"><span class="label">'. __( $custom_field['label'], 'qproject' ) .':</span><span class="description">'. $custom_field['value'] .'</span></div>';
    //         $counter++;
    //     }
    // }

    $custom_field = get_field_object( 'region_akim', $ID );

    if ( $custom_field['value'] ) {
        $content .= '<div class="region_att"><span class="label">'. __( 'Akim', 'qproject' ) .':</span><span class="description">'. $custom_field['value'] .'</span></div>';
        $counter++;
    }

    $custom_field = get_field_object( 'region_area', $ID );

    if ( $custom_field['value'] ) {
        $content .= '<div class="region_att"><span class="label">'. __( 'Area', 'qproject' ) .':</span><span class="description">'. $custom_field['value'] .'</span></div>';
        $counter++;
    }  

    $custom_field = get_field_object( 'region_population', $ID );

    if ( $custom_field['value'] ) {
        $content .= '<div class="region_att"><span class="label">'. __( 'Population', 'qproject' ) .':</span><span class="description">'. $custom_field['value'] .'</span></div>';
        $counter++;
    }  

    // $custom_field = get_field_object( 'region_timezone', $ID );

    // if ( $custom_field['value'] ) {
    //     $content .= '<div class="region_att"><span class="label">'. __( 'Timezone', 'qproject' ) .':</span><span class="description">'. $custom_field['value'] .'</span></div>';
    //     $counter++;
    // }                

    if ( !$content || ( $counter == 1 ) ) {
        $content = $region_post->post_content;
    }

    $content .= '<a href="' . get_permalink( $ID ) .'">' . __( 'Read more &raquo;', 'qproject' ) . '</a>';

    // gets the coat of arms if present - featured image
    $coat_arms = get_the_post_thumbnail( $ID, 'map-thumbnail' );

    return json_encode( array(
        'text' => $content,
        'icon' => $coat_arms
    ) );
}


function jvectormap_script() { ?>

    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/jquery-jvectormap-1.2.2.css" type="text/css" media="screen"/>

    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-jvectormap-kaz.js"></script>

    <script type='text/javascript'>
    jQuery(function(){

        <?php
        $args = array(
            'posts_per_page' => -1,
            'post_type'      => 'map'
        );
        $map_posts = get_posts( $args );
        ?>

        var array_content = [
            <?php foreach ( $map_posts  as $map_posts ) : ?>
            <?php // print_r($map_posts); ?>
                {latLng: [<?php the_field( '_region_coordinates', $map_posts->ID ); ?>], name: '<?php echo $map_posts->post_title; ?>', id: '<?php the_field( '_region_id', $map_posts->ID ); ?>', content: <?php echo get_content_map( $map_posts->ID ); ?>},
            <?php endforeach; ?>
        ];

        // City1 code----------------------------
        var result1 = jQuery.grep(array_content, function(e){ return e.name == "Астана" || e.name == "Astana"; });
        if (result1.length == 1) {
            jQuery('#city1').css('background','url(<?php echo get_stylesheet_directory_uri(); ?>/images/yellow-star.png) no-repeat 0 0 transparent');
            jQuery('#city1').append('<div class="jvectormap-label" id="tooltip1">' + result1[0].name + '</div>');
            jQuery('#tooltip1').css('bottom', 20 ).css('right', 20 );
        }

        jQuery('#city1').click(function(){
            if (result1.length == 1) {
                jQuery("#text h2").empty();
                jQuery("#text h2").append(result1[0].content.icon + '<span class="region-name">' + result1[0].name + '</span>');            
                jQuery("#text div.region-desc").empty();
                jQuery("#text div.region-desc").append(result1[0].content.text);          
            }
        });

        jQuery('#city1').mouseover(function(e) {
            jQuery(this).find("#tooltip1").css("display", "block");
        });
        jQuery('#city1').mouseout(function(e) {
            jQuery(this).find("#tooltip1").css("display", "none");
        });        

         // City2 code----------------------------
        var result2 = jQuery.grep(array_content, function(e){ return e.name == "Алматы" || e.name == "Almaty"; });
        if (result2.length == 1) {
            jQuery('#city2').css('background','url(<?php echo get_stylesheet_directory_uri(); ?>/images/small-yellow-dot.png) no-repeat 0 0 transparent');
            jQuery('#city2').append('<div class="jvectormap-label" id="tooltip2">' + result2[0].name + '</div>');
            jQuery('#tooltip2').css('bottom', 20 ).css('right', 20 );
        }

        jQuery('#city2').click(function(){
            if (result2.length == 1) {
                jQuery("#text h2").empty();
                jQuery("#text h2").append(result2[0].content.icon + '<span class="region-name">' + result2[0].name + '</span>');            
                jQuery("#text div.region-desc").empty();
                jQuery("#text div.region-desc").append(result2[0].content.text);
            }
        });

        jQuery('#city2').mouseover(function(e) {
            jQuery(this).find("#tooltip2").css("display", "block");
        });
        jQuery('#city2').mouseout(function(e) {
            jQuery(this).find("#tooltip2").css("display", "none");
        });        
        
        // ---------------------------------------

        var array_values = {
            "KZ-AA": 1,
            "KZ-AM": 2,
            "KZ-AT": 3,
            "KZ-AR": 4,
            "KZ-EK": 5,
            "KZ-MG": 6,
            "KZ-NK": 7,
            "KZ-PA": 1,
            "KZ-QG": 2,
            "KZ-QS": 3,
            "KZ-QO": 4,
            "KZ-SK": 5,
            "KZ-WK": 6,
            "KZ-ZM": 7
        };
        var map, markerIndex = 0, markersCoords = {};

        map = new jvm.WorldMap({
            container: jQuery('.map'),
            map: 'kazakhstan',
            series: {
                regions: [{
                    values: array_values,
                    //scale: ['#C8EEFF', '#0071A4'],
                    scale: ['#85D9FF', '#008395'],
                    normalizeFunction: 'polynomial'
                }]
            },
            zoomOnScroll: false,
            zoomButtons : false,
            color: '#000000',
            /*markerStyle: {
                initial: {
                    fill: '#00918E'
                }
            },*/
            regionStyle: {
                hover: {
                    "fill-opacity": 0.5
                }
            },
            backgroundColor: '#f6f6f6',
            regionsSelectable: false,
            markersSelectable: true,
            markersSelectableOne: true,
            onRegionLabelShow: function(event, label, code) {
                var result = jQuery.grep(array_content, function(e) { return e.id == code; });
                label.html(result[0].name);
                // name = label.html();
            },
            onRegionClick: function(e, code){
                var result = jQuery.grep(array_content, function(e){ ;return e.id == code; });

                jQuery("#text h2").empty();
                jQuery("#text h2").append(result[0].content.icon + '<span class="region-name">' + result[0].name + '</span>');

                if (result.length == 0) {
                    // not found
                    //console.log("not found");
                } else if (result.length == 1) {
                    // access the foo property using result[0].foo
                    //console.log("access the foo property using result[0].foo: " + result[0].content);
                    jQuery("#text div.region-desc").empty();
                    jQuery("#text div.region-desc").append(result[0].content.text);
                } else {
                    // multiple items found
                    //console.log("multiple items found");
                }
            }
        });

    }); //End function
    </script>

<?php }

add_action( 'wp_head', 'jvectormap_script' ); 
?>