<?php

/**
 *  get a custom excerpt
 *  inq_excerpt
 *
 */

if( !function_exists( 'inq_excerpt' ) ){
    function inq_excerpt($content='', $length=200, $ellipsis='') {
        $output = '';
        if ($content) {
            $output = strip_shortcodes($content);
            $output = str_replace(']]>', ']]&gt;', $output);
            $output = strip_tags($output);
            $wrap = wordwrap($output, $length, '##');
            $output = substr($wrap, 0, strpos($wrap, '##')). $ellipsis;
        }
        return $output;
    }
}

/**
 * Load jQuery cycle in the home page
 *
 */

if( !is_front_page() ){
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script('cycle', get_stylesheet_directory_uri(). '/js/jquery.cycle.all.js', array('jquery'));
    wp_enqueue_script('home-page-featured-articles', get_stylesheet_directory_uri(). '/js/home-page-featured-articles.js', array('jquery', 'cycle'));
}

/**
 * Add image size for the featured articles slider.
 *
 */

if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'homepage-thumb', 308, 190, true ); //(cropped)
}