<?php

function custom_excerpt_length($length) {
	return 15;
}

add_filter('excerpt_length', 'custom_excerpt_length', 999);

function new_excerpt_more($more) {
	return '';
}

add_filter('excerpt_more', 'new_excerpt_more');

/*
register_post_type('map', array('label' => 'Regions', 'description' => 'Regions that have specific content that is shown in the map at Home Page.', 'public' => true, 'show_ui' => true, 'show_in_menu' => true, 'capability_type' => 'post', 'hierarchical' => false, 'rewrite' => array('slug' => ''), 'query_var' => true, 'exclude_from_search' => false, 'supports' => array('title', 'editor', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'thumbnail', 'author', 'page-attributes',), 'labels' => array(
		'name' => 'Regions',
		'singular_name' => 'Region',
		'menu_name' => 'Regions',
		'add_new' => 'Add Region',
		'add_new_item' => 'Add New Region',
		'edit' => 'Edit',
		'edit_item' => 'Edit Region',
		'new_item' => 'New Region',
		'view' => 'View Region',
		'view_item' => 'View Region',
		'search_items' => 'Search Regions',
		'not_found' => 'No Regions Found',
		'not_found_in_trash' => 'No Regions Found in Trash',
		'parent' => 'Parent Region',
	),));
 */

function mrd_check_ccs_cookie() {
	if (isset($_GET['css'])) {
		switch ($_GET['css']) {
			case 'impaired':
				setcookie('mrd_theme_version', 'visually-impaired', 0, '/');
				break;

			default:
				setcookie('mrd_theme_version', 'default', 0, '/');
				break;
		}

		if (!empty($_SERVER['HTTP_REFERER'])) {
			$redirect = $_SERVER['HTTP_REFERER'];
		} else {
			$redirect = get_bloginfo('url');
		}
		header('Location: '. $redirect);
		exit;
	}

	if (!isset($_COOKIE['mrd_theme_version'])) {
		setcookie('mrd_theme_version', 'default', 0, '/');
	}
}

add_action('init', 'mrd_check_ccs_cookie');