<?php
if ( !function_exists( 'kmrd_enqueue_scripts' ) ) {
    function kmrd_enqueue_scripts() {
        wp_enqueue_style( 'kmrd-print-style', get_bloginfo( 'stylesheet_directory' ) . '/print.css', FALSE, FALSE, 'print' );
        wp_enqueue_script( 'kmrd-menu-script', get_bloginfo( 'stylesheet_directory' ) . '/js/menu.js', array( 'jquery' ), false, TRUE );
        wp_enqueue_script( 'kmrd-jcarousel-script', get_bloginfo( 'stylesheet_directory' ) . '/js/jcarousellite_1.0.1.min.js', false, TRUE );
        wp_enqueue_script( 'kmrd-script', get_bloginfo( 'stylesheet_directory' ) . '/js/ksm-kmrd.js', array( 'jquery', 'kmrd-jcarousel-script' ), false, TRUE );
        wp_enqueue_script( 'orphus', get_bloginfo( 'stylesheet_directory' ) . '/js/orphus.js', array( 'jquery' ), false, TRUE );
    }
}

add_action( 'init', 'kmrd_enqueue_scripts', 10 );