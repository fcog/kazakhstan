<?php
global $blog_id;
switch_to_blog( 1 );
$main_blog_url = get_bloginfo( 'url' );
restore_current_blog();

define( 'TIMTHUMB_URL', get_stylesheet_directory_uri() . '/scripts/timthumb.php' );

add_theme_support( 'map-thumbnail' );
add_image_size( 'map-thumbnail', 100, 100 );

function print_menu_shortcode($atts, $content = null) {
    extract(shortcode_atts(array( 'name' => null, ), $atts));
    return "<div id='sitemap'>".wp_nav_menu( array( 'menu' => $name, 'echo' => false ) )."</div>";
}
add_shortcode('menu', 'print_menu_shortcode');

if ( $blog_id == 1 ) {
    define( 'FORUM_URL', $main_blog_url . '/forum' );
    define( 'FORUM_DB_PREFIX', 'GDN_' );
} elseif ( $blog_id == 2 ) {
    define( 'FORUM_URL', $main_blog_url . '/forum/rus' );
    define( 'FORUM_DB_PREFIX', 'GDN_Rus_' );
} else {
    define( 'FORUM_URL', $main_blog_url . '/forum/eng' );
    define( 'FORUM_DB_PREFIX', 'GDN_Eng_' );
}

add_filter('get_search_form', 'my_search_form');
 
function my_search_form($text) {
     $placeholder = 'type="text" placeholder="'.__('Search','qproject').'"';
     $text = str_replace('type="text"', $placeholder, $text);
     return $text;
}

/**
 *
 */
function get_forum_discussions() {
    $context = stream_context_create(array(
    'http' => array(
        'header'  => "Authorization: Basic " . base64_encode("kaz1:kaz2")
    )
    ));
    $discussions_json = file_get_contents( FORUM_URL . '/?p=discussions.json' , false, $context);
    $discussions = json_decode( $discussions_json );

    return $discussions;
}

/**
 *
 */
function get_last_discussion() {
    global $wpdb;

    $last_discussion = $wpdb->get_row("
        SELECT D.DiscussionID, D.Name, D.Body, U.Photo AS UserPhoto, U.Name AS Username
        FROM " . FORUM_DB_PREFIX . "Discussion D
        INNER JOIN " . FORUM_DB_PREFIX . "User U ON U.UserID = D.InsertUserID
        WHERE Announce = 1
        ORDER BY D.DateInserted DESC
        LIMIT 1;
    ");

    return $last_discussion;
}

/**
 *
 */
function get_last_discussion_comment( $discussionId ) {
    global $wpdb;

    $query = $wpdb->prepare("
        SELECT Body
        FROM " . FORUM_DB_PREFIX . "Comment
        WHERE DiscussionID = %d ORDER BY CommentID DESC LIMIT 1", $discussionId );
    $last_discussion_comment = $wpdb->get_var( $query );

    return $last_discussion_comment;
}

/**
 *
 */
function get_real_photo_path( $photo ) {
    $tmp_photo = explode( '/', $photo );
    return TIMTHUMB_URL . '?src=' . FORUM_URL . '/uploads/' . $tmp_photo[0] . '/' . $tmp_photo[1] . '/p' . $tmp_photo[2] . '&cp=1&w=100&h=100';
}

function excerpt_count_js(){
      echo '<script>
              function updateCountdown(input, counter, maxchars) {
                // 140 is the max message length
                var remaining = maxchars - jQuery(input).val().length;
                jQuery(counter).val(remaining);
              }

              jQuery(document).ready(function(){

                jQuery("#title").after("<div style=\"position:absolute;bottom:63px;right:0;color:#666;\"><small>' . __('Characters left:', 'qproject') . '</small><input type=\"text\" value=\"0\" maxlength=\"3\" size=\"3\" id=\"title_counter\" readonly=\"\" style=\"background:#fff;\"></div>");
                var inputId = "#title";
                var counterId = "#title_counter";
                updateCountdown("#title", counterId, 75);
                jQuery("#title").keyup( function() { updateCountdown(this, "#title_counter", 75) } );
            
                var inputId = "#excerpt";
                var counterId = "#excerpt_counter";

                if (jQuery(inputId).length > 0){
                    jQuery("#postexcerpt .handlediv").after("<div style=\"position:absolute;top:0px;right:5px;color:#666;\"><small>' . __('Characters left:', 'qproject') . '</small><input type=\"text\" value=\"0\" maxlength=\"3\" size=\"3\" id=\"excerpt_counter\" readonly=\"\" style=\"background:#fff;\"></div>");
                    updateCountdown(inputId, counterId, 140);
                    jQuery(inputId).keyup( function() { updateCountdown(inputId, counterId, 140) } );
                }
              });
        </script>';
}
add_action( 'admin_head-post.php', 'excerpt_count_js');
add_action( 'admin_head-post-new.php', 'excerpt_count_js');