jQuery("#featured-articles").cycle({
    fx: 'fade',
    speed: 2000,
    pause: 1,
    timeout: 10000,
    prev:    '#prev',
    next:    '#next',
    pager:  '#fa-slide-nav',
    pagerAnchorBuilder: pagerFactory
});

function pagerFactory(idx, slide) {
    var s = idx > 9 ? ' style="display:none"' : '';
    return '<li'+s+'><a href="#">'+(idx+1)+'</a></li>';
};