var ww = document.body.clientWidth;

jQuery(function() {

  //-----------------------------------------------------------------------------------------------------------------
  // Disable menu parent links except home
  jQuery("#menu-main-menu li a").each(function() {
    if (jQuery(this).next().length > 0) {
      jQuery(this).addClass("parent");
    };
  });

  jQuery('ul#menu-main-menu > li a.parent').not("ul li ul a").each(function(){
    jQuery(this).attr('href', '#');
    jQuery(this).css('cursor', 'default');
  });
  //------------------------------------------------------------------------------------------------------------------

  jQuery('.tweet_list').addClass('grid_5 suffix_1');
  jQuery('.tweet_list').first().addClass('alpha');
  jQuery('.tweet_list').last().addClass('omega');

  jQuery(".tweet_area li").removeClass('tweet_list');

  jQuery('footer').find('.menu > li').addClass('grid_3');

  jQuery('form').find('input[name="log"]').each(function(ev) {
    if(!jQuery(this).val()) {
      jQuery(this).attr("placeholder", "user name");
    }
  });

  jQuery('form').find('input[name="pwd"]').each(function(ev) {
    if(!jQuery(this).val()) {
      jQuery(this).attr("placeholder", "password");
    }
  });

  jQuery('#homepage-banners').jCarouselLite({
    btnPrev: '.nav-left',
    btnNext: '.nav-right',
    visible: 4
  });
  jQuery('#region-banners').jCarouselLite({
    btnPrev: '.nav-left',
    btnNext: '.nav-right',
    visible: 3
  });  


// Suport Mobile and table menu wrap

  jQuery("#menu-trigger").click(function(e) {
      e.preventDefault();
      jQuery("#main-menu").toggle();
  });

  adjustMenu();
});

jQuery(window).bind('resize orientationchange', function() {
  ww = document.body.clientWidth;
  adjustMenu();
});

var adjustMenu = function() {
  if (ww <= 500) {

      if (!(jQuery(".more-link").length > 0)) {
        jQuery('<div class="more-link">&nbsp;</div>').insertBefore(jQuery('.parent')); 
      }
      
      jQuery("#main-menu li").unbind('mouseenter mouseleave');
      jQuery("#main-menu li a.parent").unbind('click');
      jQuery("#main-menu li .more-link").unbind('click').bind('click', function() {
        jQuery(this).parent("li").toggleClass("hover");
      });
  } 
  else {
    jQuery("#main-menu").show();
    
    if (jQuery(".more-link").length > 0) {
     jQuery('.more-link').remove(); 
    }

    jQuery("#main-menu li").removeClass("hover");
    jQuery("#main-menu li a").unbind('click');
    jQuery("#main-menu li").unbind('mouseenter mouseleave').bind('mouseenter mouseleave', function() {
      // must be attached to li so that mouseleave is not triggered when hover over submenu
      jQuery(this).toggleClass('hover');
    });
  }
}