jQuery(function() {
  // Disable menu parent links except home
  jQuery("#menu-main-menu li a").each(function() {
    if (jQuery(this).next().length > 0) {
      jQuery(this).addClass("parent");
    };
  });

  jQuery('ul#menu-main-menu > li a.parent').not("ul li ul a").each(function(){
    jQuery(this).attr('href', '#');
    jQuery(this).css('cursor', 'default');
  });	
});