<?php
/**
 * Template for the header of the website. Sets the meta information and opens up a bunch of wrapper elements.
 *
 * @package qProject
 * @see footer.php
 */
global $blog_id;
$is_homepage = is_front_page();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>" />

		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/flag_kazakhstan.ico" />

		<!-- Set the viewport width to device width for mobile -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<title><?php bloginfo('name'); ?> | <?php is_home() ? bloginfo('description') : wp_title(''); ?></title>

		<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
		<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /><![endif]-->

		<!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/less/normalize.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/less/grid.css" type="text/css" media="only screen and (min-width: 800px)" />
		<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/less/grid720.css" type="text/css" media="only screen and (max-width: 799px)"  />
		<?php if (isset($_COOKIE['mrd_theme_version']) && ($_COOKIE['mrd_theme_version'] == 'visually-impaired')): ?>
			<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/visually-impaired.css" type="text/css" media="screen" />
		<?php else: ?>
			<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
		<?php endif; ?>

		<?php wp_head(); ?>
	</head>

	<!--body id="<?php echo (is_page()) ? get_query_var('name') : ((is_home()) ? "home" : ((is_single()) ? "single" : ((is_category()) ? single_cat_title() : ((is_archive()) ? "archive" : "")))); ?>" <?php body_class(); ?>-->
	<body id="mrdsite-<?php echo $blog_id; ?>" <?php body_class(); ?>>
		<div class="container_24">
			<header>

				<div id="header">
					<div class="grid_24 header-top">
						<div class="grid_<?php echo $is_homepage ? 11 : 9; ?> alpha">
							<?php
							wp_nav_menu(array(
								'theme_location' => 'top',
//                'menu'           => 7,
								'container' => 'div',
								'container_id' => 'top-menu'
							));
							?>
						</div>

						<div class="i18n grid_<?php echo $is_homepage ? 13 : 15; ?> omega box_lenguage">
							<span class="print-box">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-mobile.png" id="mobile" alt="Mobile Enabled Website" title="Mobile Enabled Website">
								<?php if ( !$is_homepage ) : ?>
									<a href="javascript:window.print();"><?php _e( 'Print page', 'qproject' ); ?></a>&nbsp; | &nbsp;
								<?php endif; ?>

								<?php if (isset($_COOKIE['mrd_theme_version']) && ($_COOKIE['mrd_theme_version'] == 'visually-impaired')): ?>
									<a href="/?css=default"><?php _e('Default view', 'qproject'); ?></a><span class="separator">&nbsp; | &nbsp;</span>
								<?php else: ?>
									<a href="/?css=impaired"><?php _e('Visually Impaired', 'qproject'); ?></a><span class="separator">&nbsp; | &nbsp;</span>
								<?php endif; ?>
							</span>
							<span class="language-box">
								<a href="/">ҚАЗ</a> &#124;
								<a href="/rus/">РУС</a> &#124;
								<a href="/eng/">ENG</a>
							</span>
						</div>
					</div>

					<div class="grid_24 header-middle">
						<div class="grid_12 alpha">
							<h1>
								<a href="<?php bloginfo('url'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/kazakhstan-ministry-or-regional-development-logo.png" title="<?php echo __('Ministry of Regional Development of the Republic of Kazakhstan', 'qproject'); ?>" alt="<?php echo __('Ministry of Regional Development of the Republic of Kazakhstan', 'qproject'); ?>" /></a>
								<span class="title-line-1"><?php echo __('Ministry of Regional', 'qproject'); ?></span>
								<br />
								<span class="title-line-2"><?php echo __('Development of the Republic of Kazakhstan', 'qproject'); ?></span>
							</h1>
						</div>

						<div class="grid_9 omega prefix_3 box_formlogin">
							<?php
							if (is_active_sidebar('sidebar-header-right')) :
								dynamic_sidebar('sidebar-header-right');
							endif;
							?>
						</div>

						<div class="box_search">
							<?php
							if (is_active_sidebar('sidebar-header-search')) :
								dynamic_sidebar('sidebar-header-search');
							endif;
							?>
						</div>

						<div class="box_follow">
							<?php
							if (is_active_sidebar('sidebar-header-follow')) :
								dynamic_sidebar('sidebar-header-follow');
							endif;
							?>
						</div>

					</div>
					
					<div id="menu-wrap" class="grid_24 header-bottom">
						<div id="menu-trigger"><?php _e('Menu', 'qproject') ?></div>
						<?php
						wp_nav_menu(array(
							'theme_location' => 'header',
//              'menu'           => 6,
							'container' => 'nav',
							'container_id' => 'main-menu'
						));
						?>
					</div>
				</div>

			</header>

			<div class="clear"></div>

			<!-- Content Wrapper -->
			<div id="content" class="content-wrapper">
