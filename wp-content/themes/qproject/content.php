<?php
/**
 * Template for a generic post.
 *
 * @package qProject
 */
?>

<!-- Blog post -->
<article class="post">

	<h1 class="title">
		<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php echo _e( 'Permanent Link to ', 'qproject' ); the_title_attribute(); ?>"><?php the_title(); ?></a>
	</h1>
	
	<div class="info">

	</div>
	
	<div class="entry">
		<?php the_content(__('Read more', 'qproject').' &raquo;' ); ?>
	</div>
	
	<?php get_template_part( 'meta', 'index' ); ?>
	
</article>