<?php
/**
 * Template for displaying a trackback comment.
 *
 * @package qProject
 */
?>

<li class="post pingback">
	<p><?php _e( 'Pingback:', 'qproject' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( 'Edit', 'qproject' ), '<span class="edit-link">', '</span>' ); ?></p>
