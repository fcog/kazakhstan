<?php
/**
 * Generic template used when no other template is available.
 *
 * @package qProject
 */

get_header();
qproject_get_columns( 'index' );
get_footer();
