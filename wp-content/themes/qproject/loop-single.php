<?php
/**
 * Loop template for a single post.
 *
 * @package qProject
 */
?>

<!-- The loop -->
<?php if ( have_posts() ) :
while ( have_posts() ) {
	the_post();
	qproject_get_content( 'single' );
	//comments_template( '', true );
}
else: ?>
	<p><?php _e( 'Sorry, no posts match your criteria.', 'qproject' ); ?></p>
<?php endif; ?>
