<?php
/**
 * Generic loop template, used when no other loop template is available.
 *
 * @package qProject
 */
?>

<!-- The loop -->
<?php if ( have_posts() ) :
while ( have_posts() ) {
	the_post();
	qproject_get_content( 'map' );
	//comments_template( '', true );
}
else : ?>
	<p><?php _e( 'Sorry, no posts match your criteria.', 'qproject' ); ?></p>
<?php endif; ?>
