<?php
/**
 * Generic loop template, used when no other loop template is available.
 *
 * @package qProject
 */
?>

<?php if ( is_search() ) : ?>
	<!-- Search form -->
	<h2><?php _e( 'Search' ); ?></h2>
	<?php get_search_form(); ?>
<?php endif; ?>

<!-- The loop -->
<?php if ( have_posts() ) :
while ( have_posts() ) {
	the_post();
	qproject_get_content( 'index' );
}
else : ?>
	<p><?php _e( 'Sorry, no posts match your criteria.', 'qproject' ); ?></p>
<?php endif; ?>
