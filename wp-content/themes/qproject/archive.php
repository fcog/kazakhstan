<?php
/**
 * Template used when showing an archive of posts (by date, user, category, tags).
 *
 * @package qProject
 */

get_header();
qproject_get_columns( 'archive' );
get_footer();

