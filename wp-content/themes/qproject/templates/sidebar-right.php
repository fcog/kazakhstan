<?php
/**
* @package WordPress
* @subpackage qproject
* Template Name: Content + Right Sidebar
*/

get_header();
qproject_get_columns('page', 'right');
get_footer();
