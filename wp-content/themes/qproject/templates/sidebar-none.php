<?php
/**
* @package WordPress
* @subpackage qproject
* Template Name: Content without sidebars
*/

get_header();
qproject_get_columns('page', 'none');
get_footer();
