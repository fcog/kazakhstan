<?php
/**
* @package WordPress
* @subpackage qproject
* Template Name: Content + Left Sidebar
*/

get_header();
qproject_get_columns('page', 'left');
get_footer();
