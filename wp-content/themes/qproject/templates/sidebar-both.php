<?php
/**
* @package WordPress
* @subpackage qproject
* Template Name: Content + Both Sidebars
*/

get_header();
qproject_get_columns('page', 'both');
get_footer();
