<?php
/**
 * Template for showing a page.
 *
 * @package qProject
 */

get_header();
qproject_get_columns( 'page' );
get_footer();
