<?php
/**
 * Registers the sidebars available in the theme.
 *
 * @package qProject
 */

if ( !function_exists('qproject_register_sidebars') ) {
	function qproject_register_sidebars() {
		register_sidebar(array(
			'id' => 'right-sidebar',
			'name' => 'Right Sidebar',
			'before_widget' => '<div class="sidebar widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h2 class="widget-title">',
			'after_title' => '</h2>'
		));
		register_sidebar(array(
			'id' => 'left-sidebar',
			'name' => 'Left Sidebar',
			'before_widget' => '<div class="sidebar widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h2 class="widget-title">',
			'after_title' => '</h2>'
		));
		register_sidebar(array(
			'id' => 'header',
			'name' => 'Header',
			'before_widget' => '<div class="header widget grid_6">',
			'after_widget' => '</div>',
			'before_title' => '<span class="header-widget-title">',
			'after_title' => '</span>'
		));
		register_sidebar(array(
			'id' => 'footer',
			'name' => 'Footer',
			'before_widget' => '<div class="footer widget grid_6">',
			'after_widget' => '</div>',
			'before_title' => '<span class="footer-widget-title">',
			'after_title' => '</span>'
		));
		register_sidebar(array(
			'id' => 'search-area',
			'name' => 'Search Area',
			'before_widget' => '<div class="search widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h2 class="widget-title">',
			'after_title' => '</h2>'
		));
		unregister_sidebar('sidebar-region-banners');
	    register_sidebar( array(
	      'id'            => 'sidebar-region-banners',
	      'name'          => __( 'Region - Banner Sidebar', 'qproject' ),
	      'before_title'  => '<h2 class="">',
	      'after_title'   => '</h2>',
	    ) );
	}
}

add_action( 'init', 'qproject_register_sidebars', 1 );
