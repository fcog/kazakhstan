<?php
/**
 * Gets a specific comment template.
 *
 * @package qProject
 *
 * @param object $comment The comment object
 * @param array  $args    Arguments
 * @param int    $depth   Depth of the comment
 */

if ( !function_exists( 'qproject_comment' ) ) {
	function qproject_comment( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment;
		$GLOBALS['args'] = $args;
		$GLOBALS['depth'] = $depth;
		get_template_part( 'comment', $comment->comment_type );
	}
}
