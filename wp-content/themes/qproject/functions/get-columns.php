<?php 
/**
 * Builds the correct amount of columns based on the theme options.
 *
 * @package qProject
 *
 * @param string $loop (optional) Which loop to get the contents for
 */

if ( !function_exists( 'qproject_get_columns' ) ) {
	function qproject_get_columns( $loop = 'index', $sidebars = '' ) {
		$options = get_option( 'qproject_theme_options' );
		if ( empty( $sidebars ) ) {
			$sidebars = ! empty( $options['sidebars'] ) ? $options['sidebars'] : 'right';
		}
		$grid = $class = 0;
		
		// Left sidebar
		if ( 'left' === $sidebars or 'both' === $sidebars ) :
			$grid = 'both' === $sidebars ? 6 : 6;
			?>
			<aside class="grid_<?php echo $grid; ?> alpha">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Left Sidebar') ) { _e( 'Left Sidebar', 'qproject' ); } ?>
			</aside>
		<?php endif;
		
		// Main content
		switch ( $sidebars ) {
			case 'left':
				$grid = 18;
				$class = 'omega';
				break;
			case 'right':
				$grid = 16;
				$class = 'alpha';
				break;
			case 'both':
				$grid = 12;
				$class = '';
				break;
			case 'none':
			default:
				$grid = 24;
				$class = '';
				break;
		}
		?>
		<div id="qproject-blog-content" class="<?php printf( 'grid_%d %s', $grid, $class ); ?>">
			<?php
			get_template_part( 'loop', $loop );
			get_template_part( 'pagination', $loop );
			?>
		</div>
		<?php
		
		// Right Sidebar
		if ( 'right' === $sidebars or 'both' === $sidebars ) :
			$grid = 'both' === $sidebars ? 6 : 8; ?>
		<aside id="sidebar-blog" class="grid_<?php echo $grid; ?> omega">
			<?php 
			global $post;
			$post->ID;
			$pagename = get_the_title($post->ID);
			
			if ($pagename == "Press – Releases" || $pagename == "Пресс-релизы" || $pagename == "Баспасөз хабарламасы")
			{
				if ( is_active_sidebar('sidebar-right-press-release') ) { dynamic_sidebar('sidebar-right-press-release'); } 
			}
			else if ($pagename == "Publications in the media" || $pagename == "Публикации в СМИ" || $pagename == "БАҚ жарияланымдары")
			{
				if ( is_active_sidebar('sidebar-right-publications-media') ) { dynamic_sidebar('sidebar-right-publications-media'); } 
			}
			else if ($pagename == "media-reports" || $pagename == "Медиа отчеты" || $pagename == "Сөйленген сөздер, баяндамалар")
			{
				if ( is_active_sidebar('sidebar-right-media-reports') ) { dynamic_sidebar('sidebar-right-media-reports'); }
			}
			else if ($pagename == "Speeches" || $pagename == "Выступления, доклады")
			{
				if ( is_active_sidebar('sidebar-right-speeches') ) { dynamic_sidebar('sidebar-right-speeches'); } 
			}								
			else
			{
				if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Right Sidebar') ) { _e( 'Right Sidebar', 'qproject' ); } 
			}
			?>
		</aside>
		<?php endif; ?>
		<div class="clear"></div>
		<?php
	}
}
