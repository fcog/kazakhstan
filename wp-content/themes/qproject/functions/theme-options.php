<?php
/**
 * Defines the theme options. Based on the sample options page: http://themeshaper.com/2010/06/03/sample-theme-options/
 *
 * @package qProject
 */

/**
 * Init plugin options to white list our options
 */
if ( !function_exists('qproject_theme_options_init') ) {
	function qproject_theme_options_init(){
		register_setting( 'qproject_options', 'qproject_theme_options', 'qproject_theme_options_validate' );
	}
}

/**
 * Load up the menu page
 */
if ( !function_exists('qproject_theme_options_add_page') ) {
	function qproject_theme_options_add_page() {
		add_theme_page( __( 'qProject Theme Options', 'qproject' ), __( 'qProject Options', 'qproject' ), 'edit_theme_options', 'qproject_theme_options', 'qproject_theme_options_do_page' );
	}
}

/**
 * Create the options page
 */
if ( !function_exists('qproject_theme_options_do_page') ) {
	function qproject_theme_options_do_page() {
	
		if ( ! isset( $_REQUEST['settings-updated'] ) )
			$_REQUEST['settings-updated'] = false;
	
		?>
		<div class="wrap">
			<?php screen_icon(); echo "<h2>" . __( 'qProject Theme Options', 'qproject' ) . "</h2>"; ?>
	
			<?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
			<div class="updated fade"><p><strong><?php _e( 'Options saved', 'qproject' ); ?></strong></p></div>
			<?php endif; ?>
	
			<form method="post" action="options.php">
				<?php settings_fields( 'qproject_options' ); ?>
				<?php $options = get_option( 'qproject_theme_options' ); ?>
				
				<h3>Appearance</h3>
				<table class="form-table">
					
					<!-- Navigation -->
					<tr valign="top"><th scope="row"><?php _e( 'Navigation', 'qproject' ); ?></th>
						<td>
							<input id="qproject_theme_options[footer_navigation]" name="qproject_theme_options[footer_navigation]" type="checkbox" value="1" <?php checked( '1', $options['footer_navigation'] ); ?> />
							<label class="description" for="qproject_theme_options[footer_navigation]"><?php _e( 'Show footer navigation', 'qproject' ); ?></label>
						</td>
					</tr>
					
					<!-- Sidebars -->
					<?php 
					$sidebar_options = array(
						'none' => array(
							'value' => 'none',
							'label' => __( 'Single Column', 'qproject' )
						),
						'left' => array(
							'value' => 'left',
							'label' => __( 'Left Sidebar', 'qproject' )
						),
						'right' => array(
							'value' => 'right',
							'label' => __( 'Right Sidebar', 'qproject' )
						),
						'both' => array(
							'value' => 'both',
							'label' => __( 'Three Columns ', 'qproject' )
						),
					);
					$sidebars = !empty($options['sidebars']) ? $options['sidebars'] : 'right';
					?>
					<tr valign="top"><th scope="row"><?php _e( 'Columns', 'qproject' ); ?></th>
						<td>
							<fieldset><legend class="screen-reader-text"><span><?php _e( 'Columns', 'qproject' ); ?></span></legend>
							<?php
								if ( ! isset( $checked ) )
									$checked = '';
								foreach ( $sidebar_options as $option ) {
									$radio_setting = $sidebars;
	
									if ( '' != $radio_setting ) {
										if ( $sidebars == $option['value'] ) {
											$checked = "checked=\"checked\"";
										} else {
											$checked = '';
										}
									}
									?>
									<label class="description"><input type="radio" name="qproject_theme_options[sidebars]" value="<?php esc_attr_e( $option['value'] ); ?>" <?php echo $checked; ?> /> <?php echo $option['label']; ?></label><br />
									<?php
								}
							?>
							</fieldset>
						</td>
					</tr>
					
				</table>
	
				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e( 'Save Options', 'qproject' ); ?>" />
				</p>
			</form>
		</div>
		<?php
	}
}

/**
 * Sanitize and validate input. Accepts an array, return a sanitized array.
 */
if ( !function_exists('qproject_theme_options_validate') ) {
	function qproject_theme_options_validate( $input ) {
	
		// Checkbox value is either 0 or 1
		if ( ! isset( $input['footer_navigation'] ) )
			$input['footer_navigation'] = null;
		$input['footer_navigation'] = ( $input['footer_navigation'] == 1 ? 1 : 0 );
	
		// Our radio option must actually be in our array of radio options
		if ( ! isset( $input['sidebars'] ) )
			$input['sidebars'] = null;
		if ( array_search( $input['sidebars'], array( 'none', 'right', 'left', 'both' ) ) === false )
			$input['sidebars'] = null;
	
		return $input;
	}
}

add_action( 'admin_init', 'qproject_theme_options_init' );
add_action( 'admin_menu', 'qproject_theme_options_add_page' );

// adapted from http://planetozh.com/blog/2009/05/handling-plugins-options-in-wordpress-28-with-register_setting/
