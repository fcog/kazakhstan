<?php 
/**
 * Gets the appropriate template based on the post format.
 * Note: Needs to be called after the_post()
 *
 * @package qProject
 *
 * @param string $default (optional) The default template name to get
 */

if ( !function_exists( 'qproject_get_content' ) ) {
	function qproject_get_content( $default = 'single' ) {
		if ( get_post_format() ) {
			get_template_part( 'content', get_post_format() );
		} else {
			get_template_part( 'content', $default );
		}
	}
}
