<?php
/**
 * Adds init and setup functions for the theme
 *
 * @package qProject
 *
 */

if ( !function_exists( 'qproject_init' ) ) {
    function qproject_init() {
        //TODO
    }
}


if ( !function_exists( 'qproject_setup' ) ) {
    function qproject_setup() {
        // Parent theme:
        load_theme_textdomain('qproject', get_template_directory() . '/languages');

        // Child themes should use:
        // load_child_theme_textdomain( 'my-child-theme', get_stylesheet_directory() . '/languages' );
		
		// This theme uses Featured Images (also known as post thumbnails) for per-post/per-page Custom Header images
		add_theme_support( 'post-thumbnails' );

    }
}


add_action ('init', 'qproject_init');
add_action ('after_setup_theme', 'qproject_setup');
