<?php
/**
 * Registers the menus available for this theme.
 *
 * @package qProject
 */

// Register the menus
add_theme_support('nav-menus');
register_nav_menus( array(
	'top' => __( 'Top Navigation', 'qproject' ),
	'header' => __( 'Header Navigation', 'qproject' ),
	'footer' => __( 'Footer Navigation', 'qproject' )
) );
