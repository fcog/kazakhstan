<?php
/**
 * Loop template for a page.
 *
 * @package qProject
 */
?>

<!-- The loop -->
<?php if ( have_posts() ) :
while ( have_posts() ) {
	the_post();
	qproject_get_content( 'page' );
}
else: ?>
	<p><?php _e( 'Sorry, no posts match your criteria.', 'qproject' ); ?></p>
<?php endif; ?>
