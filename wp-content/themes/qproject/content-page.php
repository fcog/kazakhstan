<?php
/**
 * Template for the contents of a page.
 *
 * @package qProject
 */
?>

<!-- Page -->
<article class="post">

	<h1 class="title">
		<?php the_title(); ?>
	</h1>

	<div class="entry clearfix">
		<?php the_content(); ?>
	</div>

</article>
