<?php
/**
 * Template for the footer of the website. Closes all tags opened in the header.php file
 *
 * @package qProject
 * @see header.php
 */
?>

			</div>
			<footer>

				<!-- Footer Navigation -->
				<div class="grid_24">
					<?php
					$options = get_option( 'qproject_theme_options' );
					if ( $options['footer_navigation'] ) {
						wp_nav_menu( array(
							'theme_location' => 'footer',
							'menu_class'    => 'nav-bar',
							'container'     => 'nav'
						) );
					}
					?>
				</div>
				

				<!-- Footer Widgets -->
				<div class="grid_24">
					<aside class="grid_16 prefix_1 omega">
						<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer') ) : ?><?php endif; ?>
						<div class="clear"></div>
					</aside>

					<div id="inq-branding" class="grid_6 suffix_1">
						<a href="http://www.inqbation.com" target="_blank" title="<?php _e('DC WordPress Developer'); ?>"><?php _e('DC WordPress Developer'); ?></a> : <a href="http://www.inqbation.com" target="_blank" title="<?php _e('inQbation Digital Marketing Agency'); ?>">inQbation</a>
					</div>
				</div>
			</footer>

		</div>
		<?php wp_footer(); ?>
	</body>
</html>
