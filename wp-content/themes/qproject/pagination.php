<?php
/**
 * Generic pagination template.
 *
 * @package qProject
 */

if ( function_exists( 'emm_paginate' ) )
	emm_paginate();
