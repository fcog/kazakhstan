<?php
/**
 * Generic meta template.
 *
 * @package qProject
 */
?>

<!-- <ul class="post-meta"> -->
	<!-- Tags -->
	<!-- <li class="tags"><?php _e( 'Tagged with', 'qproject' ); ?> <?php the_tags('', ', '); ?></li> -->
	<!-- Categories -->
	<!-- <li class="category"><?php _e( 'Filed under', 'qproject' ); ?> <?php the_category(', '); ?></li> -->
	<!-- Date/Time -->
	<?php //$time = get_the_time('U'); ?>
	<!-- <li class="time"><?php _e( 'Posted on', 'qproject' ); ?> <time datetime="<?php echo date('c', $time); ?>"><?php echo date('F jS, Y', $time) ?></time></li> -->
	<!-- Author -->
	<!-- <li class="author"><?php _e( 'Written by', 'qproject' ); ?> <?php the_author_posts_link() ?></li> -->
	<!-- Comments -->
	<!-- <li class="comments"><?php comments_popup_link( __( 'Leave a comment', 'qproject' ), __( '1 Comment', 'qproject' ), __( '% Comments', 'qproject' ) ); ?></li> -->
<!-- </ul> -->