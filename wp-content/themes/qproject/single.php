<?php
/**
 * Template for showing a single post.
 *
 * @package qProject
 */

get_header();
qproject_get_columns( 'single' );
get_footer();
