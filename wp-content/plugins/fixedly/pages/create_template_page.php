    <div id="fixedly-media-gallery-cont">

        <h3>Fixedly / Manage your Templates</h3>

        <h4>1. Select a Template</h4>
        
        <?php if (!empty($success_text)) : ?>
        <div class="updated">
            <p class="success"><strong><?php print $success_text;?></strong></p>
        </div>
        <?php endif;?>
        
        <form id="select-template" method="post" action="<?php print $_SERVER[REQUEST_URI];?>">

            <input type="hidden" name="action" value="update_template" />
            <input type="hidden" name="template_id" value="<?php print $_GET['template_id'];?>" />

            <table class="fixedly-media-gallery-table" cellspacing="1">
                <tr>
                    <th class="aleft">Title</th>
                </tr>
                <tr>
                    <td>
                        <div class="fixedly-media-gallery-field">
                            <select id="template" name="template" class="fs required">
                                <option value="">-- select one --</option>
                                <?php fixedly_get_template_list($_GET["template_id"]);?>
                            </select><br />
                            <span><em>Select a template that you want to edit and update.</em></span><br />
                        </div>
                    </td>
                </tr>
            </table>

            <?php if (isset($_GET["template_id"])) : ?>
            
            <h4>2. Add Style to your Template</h4>
            
            <div class="default-tmpl-cont">
        
                <table class="fixedly-media-gallery-table" cellspacing="1">
                    <tr>
                        <th class="aleft">Container Width</th>
                        <th class="aleft">Image/Video Width</th>
                        <th class="aleft">Height</th>
                        <?php if (in_array($_GET["template_id"], array(3,4,7,8))) : ?>
                        <th class="aleft">Left Column Width</th>
                        <th class="aleft">Right Column Width</th>
                        <?php endif;?>
                        <?php if (in_array($_GET["template_id"], array(2,7,8))) : ?>
                        <th class="aleft"># Thumbnails</th>
                        <?php endif;?>
                        <?php if (in_array($_GET["template_id"], array(1,2))) : ?>
                        <th class="aleft">Show/Hide Title & Description</th>
                        <th class="aleft">Show/Hide Prev/Next Navigation</th>
                        <?php endif;?>
                    </tr>
                    <tr>
                        <td>
                            <div class="fixedly-media-gallery-field">
                                <input type="text" name="template-width" value="<?php print $output['template-width'];?>" class="ftxt required" maxlength="128" /><br />
                                <span><em>Enter the width of your template container (it should be in px or %).</em></span><br />
                            </div>
                        </td>
                        <td>
                            <div class="fixedly-media-gallery-field">
                                <input type="text" name="template-inner-width" value="<?php print $output['template-inner-width'];?>" class="ftxt required" maxlength="128" /><br />
                                <span><em>Enter the width of your image/video (it should be in px or %).</em></span><br />
                            </div>
                        </td>
                        <td>
                            <div class="fixedly-media-gallery-field">
                                <input type="text" name="template-height" value="<?php print $output['template-height'];?>" class="ftxt required" maxlength="128" /><br />
                                <span><em>Enter the height of your template (it should be only in px).</em></span><br />
                            </div>
                        </td>
                        <?php if (in_array($_GET["template_id"], array(3,4,7,8))) : ?>
                        <td>
                            <div class="fixedly-media-gallery-field">
                                <input type="text" name="template-left-width" value="<?php print $output['template-left-width'];?>" class="ftxt required" maxlength="128" /><br />
                                <span><em>Enter the height of your template left column (it should be in px or %).</em></span><br />
                            </div>
                        </td>
                        <td>
                            <div class="fixedly-media-gallery-field">
                                <input type="text" name="template-right-width" value="<?php print $output['template-right-width'];?>" class="ftxt required" maxlength="128" /><br />
                                <span><em>Enter the height of your template right column (it should be in px or %).</em></span><br />
                            </div>
                        </td>
                        <?php endif;?>
                        <?php if (in_array($_GET["template_id"], array(2,7,8))) : ?>
                        <td>
                            <div class="fixedly-media-gallery-field">
                                <input type="text" name="template-thumbnails" value="<?php print $output['template-thumbnails'];?>" class="ftxt required" maxlength="128" /><br />
                                <span><em>Enter the number of thumbnails you want to see in your slider.</em></span><br />
                            </div>
                        </td>
                        <?php endif;?>
                        <?php if (in_array($_GET["template_id"], array(1,2))) : ?>
                        <td>
                            <div class="fixedly-media-gallery-field">
                                <input type="checkbox" name="template-hide-title-desc" value="checked" <?php if ($output['template-hide-title-desc'] == "checked") print "checked";?> />
                                <span><em>Check if you want to hide the title & description of your template.</em></span><br />
                            </div>
                        </td>
                        <td>
                            <div class="fixedly-media-gallery-field">
                                <input type="checkbox" name="template-hide-prevnext-nav" value="checked" <?php if ($output['template-hide-prevnext-nav'] == "checked") print "checked";?> />
                                <span><em>Check if you want to hide the prev/next navigation of your template.</em></span><br />
                            </div>
                        </td>
                        <?php endif;?>
                    </tr>
                </table>
                
                <br />
                
                <table class="fixedly-media-gallery-table" cellspacing="1">
                    <tr>
                        <th class="aleft">Container Background Color</th>
                        <th class="aleft">Container Shadow Color</th>
                        <th class="aleft">Container Border Color</th>
                    </tr>
                    <tr>
                        <td>
                            <div class="fixedly-media-gallery-field">
                                <input type="text" id="cp1" name="template-cont-background-color" value="<?php print $output['template-cont-background-color'];?>" class="ftxt" maxlength="128" readonly /><br />
                                <span><em>Select your container background color.</em></span><br />
                            </div>
                        </td>
                        <td>
                            <div class="fixedly-media-gallery-field">
                                <input type="text" id="cp2" name="template-cont-shadow-color" value="<?php print $output['template-cont-shadow-color'];?>" class="ftxt" maxlength="128" readonly /><br />
                                <span><em>Select your container shadow color.</em></span><br />
                            </div>
                        </td>
                        <td>
                            <div class="fixedly-media-gallery-field">
                                <input type="text" id="cp3" name="template-cont-border-color" value="<?php print $output['template-cont-border-color'];?>" class="ftxt" maxlength="128" readonly /><br />
                                <span><em>Select your container border color.</em></span><br />
                            </div>
                        </td>
                    </tr>
                </table>
                
                <br />
                
                <table class="fixedly-media-gallery-table" cellspacing="1">
                    <tr>
                        <th class="aleft">Content Background Color</th>
                        <th class="aleft">Content Text</th>
                    </tr>
                    <tr>
                        <td>
                            <div class="fixedly-media-gallery-field">
                                <input type="text" id="cp4" name="template-meta-background-color" value="<?php print $output['template-meta-background-color'];?>" class="ftxt" maxlength="128" readonly /><br />
                                <span><em>Select your content block background color.</em></span><br />
                            </div>
                        </td>
                        <td>
                            <div class="fixedly-media-gallery-field">
                                <input type="text" id="cp5" name="template-meta-text-color" value="<?php print $output['template-meta-text-color'];?>" class="ftxt" maxlength="128" readonly /><br />
                                <span><em>Select your content block text color.</em></span><br />
                            </div>
                        </td>
                    </tr>
                </table>
                
                <br />

                <table class="fixedly-media-gallery-table" cellspacing="1">
                    <tr>
                        <th class="aleft">Navigation Background Color</th>
                        <th class="aleft">Navigation Shadow Color</th>
                        <th class="aleft">Navigation Border Color</th>
                    </tr>
                    <tr>
                        <td>
                            <div class="fixedly-media-gallery-field">
                                <input type="text" id="cp6" name="template-nav-background-color" value="<?php print $output['template-nav-background-color'];?>" class="ftxt" maxlength="128" readonly /><br />
                                <span><em>Select your prev/next navigation buttons background color.</em></span><br />
                            </div>
                        </td>
                        <td>
                            <div class="fixedly-media-gallery-field">
                                <input type="text" id="cp7" name="template-nav-shadow-color" value="<?php print $output['template-nav-shadow-color'];?>" class="ftxt" maxlength="128" readonly /><br />
                                <span><em>Select your prev/next navigation buttons shadow color.</em></span><br />
                            </div>
                        </td>
                        <td>
                            <div class="fixedly-media-gallery-field">
                                <input type="text" id="cp8" name="template-nav-border-color" value="<?php print $output['template-nav-border-color'];?>" class="ftxt" maxlength="128" readonly /><br />
                                <span><em>Select your prev/next navigation buttons border color.</em></span><br />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

            <?php if ($_GET["template_id"] != 9) : ?>
            
            <h4>3. Add Effects to your Template Slider</h4>

            <div class="default-tmpl-cont">

                <table class="fixedly-media-gallery-table" cellspacing="1">
                    <tr>
                        <th class="aleft">Effect</th>
                        <th class="aleft">Speed</th>
                        <th class="aleft">Delay</th>
                        <th class="aleft">Start</th>
                        <th class="aleft">Random</th>
                        <th class="aleft">Timeout</th>
                    </tr>
                    <tr>
                        <td>
                            <div class="fixedly-media-gallery-field">
                                <select name="slider-effect" class="fs required">
                                    <option value="">-- select one --</option>
                                    <?php fixedly_get_template_effects($output['slider-effect']);?>
                                </select><br />
                                <span><em>Name of transition effect.</em></span><br />
                            </div>
                        </td>
                        <td>
                            <div class="fixedly-media-gallery-field">
                                <input type="text" name="slider-speed" value="<?php print $output['slider-speed'];?>" class="ftxt required" maxlength="128" /><br />
                                <span><em>Speed of the transition (any valid fx speed value).</em></span><br />
                            </div>
                        </td>
                        <td>
                            <div class="fixedly-media-gallery-field">
                                <input type="text" name="slider-delay" value="<?php print $output['slider-delay'];?>" class="ftxt required" maxlength="128" /><br />
                                <span><em>Additional delay (in ms) for first transition (hint: can be negative) .</em></span><br />
                            </div>
                        </td>
                        <td>
                            <div class="fixedly-media-gallery-field">
                                <input type="text" name="slider-start" value="<?php print $output['slider-start'];?>" class="ftxt required" maxlength="128" /><br />
                                <span><em>Zero-based index of the first slide to be displayed.</em></span><br />
                            </div>
                        </td>
                        <td>
                            <div class="fixedly-media-gallery-field">
                                <input type="text" name="slider-random" value="<?php print $output['slider-random'];?>" class="ftxt required" maxlength="128" /><br />
                                <span><em>True for random, false for sequence.</em></span><br />
                            </div>
                        </td>
                        <td>
                            <div class="fixedly-media-gallery-field">
                                <input type="text" name="slider-timeout" value="<?php print $output['slider-timeout'];?>" class="ftxt required" maxlength="128" /><br />
                                <span><em>Milliseconds between slide transitions (0 to disable auto advance.</em></span><br />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <?php endif;?>

            <p><input type="submit" class="button-primary" value="<?php _e('Update Template');?>" /></p>
            <?php endif;?>
        </form>
        
    </div>
