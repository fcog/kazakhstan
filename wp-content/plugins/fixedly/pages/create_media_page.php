

    <div id="fixedly-media-gallery-cont">

        <h3>Fixedly / Manage your Media</h3>

        <h4>1. Select your Gallery</h4>
        
        <form id="add-new-media" method="post" action="<?php print $_SERVER[REQUEST_URI];?>">
        
            <input type="hidden" name="action" value="add_new_media" />
        
            <table class="fixedly-media-gallery-table" cellspacing="1">
                <tr>
                    <th class="aleft">Gallery Title</th>
                </tr>
                <tr>
                    <td class="aleft">
                        <div class="fixedly-media-gallery-field">
                            <select name="gid" class="fs required">
                                <option value="">-- select one --</option>
                                <?php fixedly_get_gallery_list();?>
                            </select><br />
                            <span><em>Select the gallery where you are going to add your media.</em></span>
                        </div>
                    </td>
                </tr>
            </table>
        
            <h4>2. Add New Media</h4>
            
            <?php if (!empty($success_text)) :?>
            <div class="updated">
                <p class="success"><strong><?php print $success_text;?></strong></p>
            </div>
            <?php endif;?>
            
            <p><a href="javascript:void(0);" class="add-another-row">+ Add another row...</a></p>
            
            <table id="add-media-table" class="fixedly-media-gallery-table" cellspacing="1">
                <tr id="add-media-table-head">
                    <th class="aleft">Media Title *</th>
                    <th class="aleft">Media Description *</th>
                    <th class="aleft">My media type would be: *</th>
                    <th class="aleft">Media Link</th>
                    <th class="aleft">Upload Media</th>
                </tr>
                <tr>
                    <td>
                        <div class="fixedly-media-gallery-field">
                            <input type="text" name="title[]" value="" class="ftxt required" maxlength="255" /><br />
                            <span><em>Enter the title of your media (cannot exceed 128 characters).</em></span>
                        </div>
                    </td>
                    <td>
                        <div class="fixedly-media-gallery-field">
                            <textarea name="description[]" class="fta required"></textarea>
                            <span><em>Enter a short description of your media.</em></span>
                        </div>
                    </td>
                    <td>
                        <div class="fixedly-media-gallery-field">
                            <select name="type[]" class="fs required">
                                <option value="">-- select one --</option>
                                <option value="image">an image</option>
                                <option value="vimeo">a video from Vimeo</option>
                                <option value="youtube">a video from YouTube</option>
                                <option value="dailymotion">a video from Dailymotion</option>
                            </select><br />
                            <span><em>Select the type of your media.</em></span><br />
                        </div>
                    </td>
                    <td>
                        <div class="fixedly-media-gallery-field">
                            <input class="ftxt" type="text" name="link[]" value="" maxlength="255" /><br />
                            <span><em>Enter the image URL or video ID.</em></span><br /><br />
                            URL: http://www.google.com/<br />
                            Vimeo: http://vimeo.com/<strong>48463907</strong><br />
                            YouTube: http://www.youtube.com/watch?v=<strong>shbgRyColvE</strong><br />
                            Dailymotion: http://www.dailymotion.com/video/<strong>x3bid_cars_news</strong><br />
                        </div>
                    </td>
                    <td class="acenter">
                        <div class="fixedly-media-gallery-field">
                            <input class="fixedly_upload_image" type="hidden" name="screenshot[]" value="" maxlength="255" />
                            <input class="fixedly_upload_image_button" type="button" value="Upload &amp; Attach Image" /><br /><br />
                            <span class="show_screenshot"></span>
                        </div>
                    </td>
                </tr>
            </table>

            <p><input type="submit" class="button-primary" value="<?php _e('Add New Media');?>" /></p>
            
            <p><sup>*</sup> this field is required</p>
        </form>
    </div>
