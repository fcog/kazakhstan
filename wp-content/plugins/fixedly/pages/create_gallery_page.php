

    <div id="fixedly-media-gallery-cont">
    
        <h3>Fixedly / Manage your Galleries</h3>

        <h4>1. Add New Gallery</h4>
        
        <?php if (!empty($success_text)) :?>
        <div class="updated">
            <p class="success"><strong><?php print $success_text;?></strong></p>
        </div>
        <?php endif;?>
        
        <form id="add-new-gallery" method="post" action="<?php print $_SERVER[REQUEST_URI];?>">
        
            <input type="hidden" name="action" value="add_new_gallery" />

            <table class="fixedly-media-gallery-table" cellspacing="1">
                <tr>
                    <th class="aleft">Title</th>
                    <th class="aleft">Description</th>
                    <th class="aleft">My gallery media would be:</th>
                    <th class="aleft">My gallery layout would be:</th>
                </tr>
                <tr>
                    <td>
                        <div class="fixedly-media-gallery-field">
                            <input type="text" name="title" value="" class="ftxt required" maxlength="128" /><br />
                            <span><em>Enter title of your new gallery (cannot exceed 128 characters).</em></span><br />
                        </div>
                    </td>
                    <td>
                        <div class="fixedly-media-gallery-field">
                            <input type="text" name="description" value="" class="ftxt" maxlength="255" /><br />
                            <span><em>Enter short description of your gallery (cannot exceed 255 characters).</em></span><br />
                        </div>
                    </td>
                    <td>
                        <div class="fixedly-media-gallery-field">
                            <select name="type" class="fs required">
                                <option value="">-- select one --</option>
                                <option value="image">with images</option>
                                <option value="video">with videos</option>
                                <option value="mixed">with images and videos</option>
                            </select><br />
                            <span><em>Select the type of your gallery.</em></span><br />
                        </div>
                    </td>
                    <td>
                        <div class="fixedly-media-gallery-field">
                            <select name="template" class="fs required">
                                <option value="">-- choose one --</option>
                                <?php fixedly_get_template_list();?>
                            </select><br />
                            <span><em>Select a template, look &amp; feel, of your gallery.</em></span><br />
                        </div>
                    </td>
                </tr>
            </table>

            <p><input type="submit" class="button-primary" value="<?php _e('Add New Gallery');?>" /></p>
        </form>

        
        <h4>2. Edit &amp; Update your Gallery</h4>
        
        <table class="fixedly-media-gallery-table" cellspacing="1">
            <tr>
                <th>Gallery ID <sup>[1]</sup></th>
                <th>Gallery # Entries</th>
                <th class="aleft">Gallery Title <sup>[2]</sup></th>
                <th class="aleft">Gallery Description <sup>[2]</sup></th>
                <th nowrap>Gallery Type <sup>[2]</sup></th>
                <th nowrap>Gallery Template <sup>[2]</sup></th>
                <th nowrap>Current Status <sup>[2]</sup></th>
                <th nowrap>Created On</th>
            </tr>
            <?php fixedly_select_galleries();?>
        </table>
        
        <?php if (!empty($_GET["gid"]) && $_GET["action"] == "manage_gallery"):?>

        <h4>3. Manage Gallery Entries</h4>
        
        <table class="fixedly-media-gallery-table" cellspacing="1">
            <tr>
                <th>Media ID</th>
                <th>Order ID <sup>[2]</sup></th>
                <th>Media Screenshot</th>
                <th class="aleft">Media Title <sup>[2]</sup></th>
                <th class="aleft">Media Link <sup>[2]</sup></th>
                <th class="aleft">Media Description <sup>[2]</sup></th>
                <th nowrap>Media Type</th>
                <th nowrap>Current Status <sup>[2]</sup></th>
                <th nowrap>Created On</th>
            </tr>
            <?php fixedly_select_media($_GET["gid"], $_GET["cpage"]);?>
        </table>
        
        <br />
        
        <p class="acenter"><?php fixedly_paginate_media($_GET["gid"], $_GET["cpage"]);?></p>
            
        <?php endif;?>

        <p><sup>[1]</sup> click on the Gallery ID to view entries<br />
            <sup>[2]</sup> all values with <abbr>dotted</abbr> line are editable</p>
    </div>
