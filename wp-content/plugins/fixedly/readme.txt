=== Fixedly Media Gallery ===
Contributors: kobra12
Donate link: http://www.fixedly.net/donate
Tags: image, images, video, slideshow, gallery, plugin, shortcode, wordpress, post, page
Requires at least: 3.2
Tested up to: 3.4.1
Stable tag: 4.3
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Create and integrate, easily and quickly your next video, image or slideshow gallery into your WordPress pages and posts.

== Description ==

Fixedly Media Gallery is WordPress plugin that can help you create and integrate, easily and quickly your next video, image or
slideshow gallery into your pages and posts. Within 3 easy steps you can create and insert a gallery to your next post.
Check out our [Screencast page](http://www.fixedly.net/screencasts/ "Screencast page") to learn more on how to use the plugin.

Be sure that you have `<?php wp_head();?>` function included into your WordPress theme header file otherwise the Fixedly Media Gallery won't work.

= Shortag =

[fixedly-media-gallery]

= Options =

* *id* - the ID of the gallery you want to insert (**required**)
* *template_id* - overwrite the default gallery template (optional)

(e.g. if you would like to have same gallery on different pages with different template style)

1 - default, 2 - default_thumbnails, 3 - content_left, 4 - content_right, 5 - content_top, 6 - content_bottom, 7 - content_left_thumbnails, 8 - content_right_thumbnails, 9 - gallery

= Examples =

`[fixedly-media-gallery id="1"]`
`[fixedly-media-gallery id="1" template_id="4"]`

= PHP Code =

Here is the code if you want to add the gallery directly into your PHP templates.

`<?php
    if (function_exists("fixedly_media_gallery")) {
        print fixedly_media_gallery("1");
    }
?>`

Another way to add gallery into your PHP templates is by using the `<?php do_shortcode();?>` function.

`<?php print do_shortcode("[fixedly-media-gallery id="1"]");?>`

== Installation ==

1. Upload `fixedly` directory to the `/wp-content/plugins/` directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Use shortcode [fixedly-media-gallery id="1"] OR PHP code
    `<?php if (function_exists("fixedly_media_gallery")) { print fixedly_media_gallery("1");}?>` OR
    `<?php print do_shortcode("[fixedly-media-gallery id="1"]");?>` in your templates.

Learn more about the plugin installation and usage @ [Fixedly Media Gallery](http://www.fixedly.net/ "Fixedly Media Gallery") website.

== Frequently Asked Questions ==

See Fixedly Media Gallery FAQs [here](http://www.fixedly.net/faqs/ "here").

== Screenshots ==

1. With FMG you have full control over all of your gallery media files. Update titles, link & description and change the order of your media files with one click.
2. Add one or more media files at once, add images, videos from Vimeo, YouTube and Dailymotion.
3. With FMG you can update and changes the style of you templates easily and quickly. There are 9 different template layouts.

See Fixedly Media Gallery more [here](http://www.fixedly.net/demos/ "demos") or  [here](http://www.fixedly.net/screencasts/ "screencasts").

== Changelog ==

= 1.3-beta =
* Added totally new structure of the plugin database (older database tables dropped)
* Added new way of handling templates with 9 unique templates
* Simplified the way to assign and update template style and effect
* Put all different types (image, video, slide) together on one create page
* Now you would able to add multiple media at once
* Added the functionality to change order of the slides
* Fixed upload & attach image issues with thickbox

= 1.2-beta =
* Reorganized the edit and create template stylesheet
* Updaded fancybox script effects
* Added to be able to edit width and height of video gallery slider thumbnails
* Added default templates during installation for image gallery (2 column) and video gallery layouts

= 1.1.1-beta =
* Added pagination to for 'Media Entries'
* Updated default values for templates
* Fixed 'Update Media Status' column (not working)
* Renamed and reorganized navigation menu and titles

= 1.1-beta =
* Added three different layouts when create new template
* Added a way to overwerite template name and type when using shortcode
* Added option to manage layout styles and able to change effects for your gallery transition
* Added option to be able to display or hide media title, description & navigation
* Added easier way to integrate and manage template
* Fixed typos and renamed and reorganized navigation menu and titles

= 1.0-beta =
* Fixedly Media Gallery 1.0 Released July 7, 2012

== Upgrade Notice ==

= 1.3 =
* Fixedly Media Gallery 1.3 (beta version) - Update to readme.txt #13
* Fixedly Media Gallery 1.3 (beta version) - Update to readme.txt #12
* Fixedly Media Gallery 1.3 (beta version) - Update to readme.txt #11
* Fixedly Media Gallery 1.3 (beta version) - Update to readme.txt #10
* Fixedly Media Gallery 1.3 (beta version) - Update to readme.txt #9
* Fixedly Media Gallery 1.3 (beta version) - Update to readme.txt #8
* Fixedly Media Gallery 1.3 (beta version) - Update to readme.txt #7
* Fixedly Media Gallery 1.3 (beta version) - Update to readme.txt #6
* Fixedly Media Gallery 1.3 (beta version) - Update to readme.txt #5
* Fixedly Media Gallery 1.3 (beta version) - Update to readme.txt #4
* Fixedly Media Gallery 1.3 (beta version) - Update to readme.txt #3
* Fixedly Media Gallery 1.3 (beta version) - Update to readme.txt #2
* Fixedly Media Gallery 1.3 (beta version)

= 1.2 =
* Fixedly Media Gallery 1.2 (beta version) - Update to readme.txt #3
* Fixedly Media Gallery 1.2 (beta version) - Update to readme.txt #2
* Fixedly Media Gallery 1.2 (beta version)

= 1.1.1 =
* Fixedly Media Gallery 1.1.1 (beta version) - Update to readme.txt #4
* Fixedly Media Gallery 1.1.1 (beta version) - Update to readme.txt #3
* Fixedly Media Gallery 1.1.1 (beta version) - Update to readme.txt #2
* Fixedly Media Gallery 1.1.1 (beta version)

= 1.1 =
* Fixedly Media Gallery 1.1 (beta version) - Update to readme.txt #3
* Fixedly Media Gallery 1.1 (beta version) - Update to readme.txt #2
* Fixedly Media Gallery 1.1 (beta version)

= 1.0 =
* Fixedly Media Gallery 1.0 (beta version) - Update to readme.txt #2
* Fixedly Media Gallery 1.0 (beta version)
