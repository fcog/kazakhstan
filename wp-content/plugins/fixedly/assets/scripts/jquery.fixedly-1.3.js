    jQuery(document).ready(function(){

        jQuery("#add-new-gallery").validate();
        jQuery("#add-new-media").validate();
        jQuery("#select-template").validate();
        
        jQuery(".fixedly-media-gallery-field").click(function() {
        
            jQuery("label.error", this).hide();
        });
        
        jQuery(".edit-gallery-title").editable("admin.php?page=fixedly/fixedly.php&do=save_gallery_title", {
            submit    : "Save",
            indicator : "Saving...",
            tooltip   : "Click to edit gallery title..."
        });
        
        jQuery(".edit-gallery-description").editable("admin.php?page=fixedly/fixedly.php&do=save_gallery_description", {
            type      : "textarea",
            submit    : "Save",
            indicator : "Saving...",
            tooltip   : "Click to edit gallery description..."
        });
        
        jQuery(".edit-gallery-type").editable("admin.php?page=fixedly/fixedly.php&do=save_gallery_type", {
            data      : "{'image' : 'with images', 'video' : 'with video', 'mixed' : 'with images and videos'}",
            type      : "select",
            submit    : "Save",
            indicator : "Saving...",
            tooltip   : "Click to edit gallery type..."
        });
        
        jQuery(".edit-gallery-template").editable("admin.php?page=fixedly/fixedly.php&do=save_gallery_template", {
            loadurl   : "admin.php?page=fixedly/fixedly.php&do=load_gallery_templates",
            type      : "select",
            submit    : "Save",
            indicator : "Saving...",
            tooltip   : "Click to edit gallery template..."
        });
        
        jQuery(".edit-gallery-status").editable("admin.php?page=fixedly/fixedly.php&do=save_gallery_status", {
            data      : "{'visible' : 'show', 'hidden' : 'hide', 'deleted' : 'delete'}",
            type      : "select",
            submit    : "Save",
            indicator : "Saving...",
            tooltip   : "Click to edit gallery status..."
        });
        
        jQuery(".edit-media-order").editable("admin.php?page=fixedly/fixedly.php&do=save_media_order", {
            submit    : "Save",
            indicator : "Saving...",
            tooltip   : "Click to edit media order ID..."
        });
        
        jQuery(".edit-media-title").editable("admin.php?page=fixedly/fixedly.php&do=save_media_title", {
            submit    : "Save",
            indicator : "Saving...",
            tooltip   : "Click to edit media title..."
        });
        
        jQuery(".edit-media-link").editable("admin.php?page=fixedly/fixedly.php&do=save_media_link", {
            submit    : "Save",
            indicator : "Saving...",
            tooltip   : "Click to edit media link..."
        });
        
        jQuery(".edit-media-description").editable("admin.php?page=fixedly/fixedly.php&do=save_media_description", {
            type      : "textarea",
            submit    : "Save",
            indicator : "Saving...",
            tooltip   : "Click to edit media description..."
        });
        
        jQuery(".edit-media-status").editable("admin.php?page=fixedly/fixedly.php&do=save_media_status", {
            data      : "{'visible' : 'show', 'hidden' : 'hide', 'deleted' : 'delete'}",
            type      : "select",
            submit    : "Save",
            indicator : "Saving...",
            tooltip   : "Click to edit media status..."
        });

        jQuery(".add-another-row").click(function() {

            jQuery('<tr><td><div class="fixedly-media-gallery-field"><input type="text" name="title[]" value="" class="ftxt" maxlength="255" /><br /><span><em>Enter the title of your media (cannot exceed 128 characters).</em></span><br /><br /> <a href="javascript:void(0)" class="delete-row">- Delete this row...</a> <strong></strong></div></td><td><div class="fixedly-media-gallery-field"><textarea name="description[]" class="fta"></textarea><span><em>Enter a short description of your media.</em></span></div></td><td><div class="fixedly-media-gallery-field"><select name="type[]" class="fs"><option value="">-- select one --</option><option value="image">an image</option><option value="vimeo">a video from Vimeo</option><option value="youtube">a video from YouTube</option><option value="dailymotion">a video from Dailymotion</option></select><br /><span><em>Select the type of your media.</em></span><br /></div></td><td><div class="fixedly-media-gallery-field"><input class="ftxt" type="text" name="link[]" value="" maxlength="255" /><br /><span><em>Enter your media link or click upload and then insert your image</em></span></div></td><td class="acenter"><div class="fixedly-media-gallery-field"><input class="fixedly_upload_image" type="hidden" name="screenshot[]" value="" maxlength="255" /><input class="fixedly_upload_image_button" type="button" value="Upload &amp; Attach Image" /><br /><br /><span class="show_screenshot"></span></div></td></tr>').insertAfter("table#add-media-table > tbody > tr#add-media-table-head");
        });
        
        jQuery(".delete-row").live("click", function(){
        
            /* Better index-calculation from @activa */
            var myIndex = jQuery(this).closest("td").prevAll("td").length + 1;
            
            jQuery(this).parents("table").find("tr:eq("+myIndex+")").remove();
        });

        
        var orig_send_to_editor = window.send_to_editor;

        jQuery(".fixedly_upload_image_button").live("click", function() {

            formfield = jQuery(this).prev("input");
            showfield = jQuery(this).next().next().next("span");
            
            tb_show("Fixedly Media Gallery", "media-upload.php?type=image&TB_iframe=true&tab=library&width=640&height=640");

            window.send_to_editor = function(html) {

                var regex = /src="(.+?)"/;
                var rslt = html.match(regex);
                var imgurl = rslt[1];
                formfield.val(imgurl);
                tb_remove();

                showfield.html('<img src="' + imgurl + '" width="150" height="150" />');
                window.send_to_editor = orig_send_to_editor;
            }

            return false;
        });
        
        jQuery("#cp1, #cp2, #cp3, #cp4, #cp5, #cp6, #cp7, #cp8").ColorPicker({
        
            onSubmit: function(hsb, hex, rgb, el) {
                jQuery(el).val(hex);
                jQuery(el).ColorPickerHide();
            },
            onBeforeShow: function () {
                jQuery(this).ColorPickerSetColor(this.value);
            }
        }).bind('keyup', function(){
            jQuery(this).ColorPickerSetColor(this.value);
        });
        
        jQuery("select#template").change(function() {

            document.location = "admin.php?page=manage_template&template_id=" + jQuery("select#template").val();
        });
    });
