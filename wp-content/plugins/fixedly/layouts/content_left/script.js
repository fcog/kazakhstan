jQuery(document).ready(function(){

    jQuery(".fixedly-slider-cont-{%post-id%}").cycle({

        fx: "{%slider-effect%}",
        pause: true,
        random: {%slider-random%},
        timeout: {%slider-timeout%},
        speed: {%slider-speed%},
        startingSlide: {%slider-start%},
        delay: {%slider-delay%},
        prev: ".fixedly-nav-prev-{%post-id%} a",
        next: ".fixedly-nav-next-{%post-id%} a"
    });

    jQuery(".fixedly-meta-cont-{%post-id%}").cycle({

        fx: "fade",
        pause: true,
        timeout: 0,
        speed: 0,
        prev: ".fixedly-nav-prev-{%post-id%} a",
        next: ".fixedly-nav-next-{%post-id%} a"
    });
});
