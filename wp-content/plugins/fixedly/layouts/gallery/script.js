jQuery(document).ready(function() {

    jQuery(".fixedly-media-gallery").fancybox({

        prevEffect : 'none',
        nextEffect : 'none',
        closeBtn   : true,
        helpers: {
            media   : true,
            title   : { type : 'inside' },
            buttons : {}
        }
    });
});
