<?php

    global $wpdb;
    global $wp_query;

    $media_table_name    = FIXEDLY_MEDIA_DB_TABLE;
    $gallery_table_name  = FIXEDLY_GALLERY_DB_TABLE;
    $template_table_name = FIXEDLY_TEMPLATE_DB_TABLE;

    $rows_affected = $wpdb->get_results("SELECT $media_table_name.*
        FROM $media_table_name, $gallery_table_name
            WHERE $media_table_name.gallery_id = '" . $gallery_id . "'
                AND $media_table_name.status = 'visible'
                    AND $gallery_table_name.status = 'visible'
                        AND $media_table_name.gallery_id = $gallery_table_name.id
                            AND $gallery_table_name.status = 'visible'
                                ORDER BY order_id ASC;");
?>


<?php if (!empty($rows_affected)) : ?>

<div class="fixedly-cont-<?php print $wp_query->post->ID;?>">
    
<?php for ($row = 0; $row < sizeof($rows_affected); $row++) : ?>
    
    <?php if ($row == 0) : ?>
    <div class="fixedly-row-<?php print $wp_query->post->ID;?> clearfix">
    <?php endif;?>
        
    <?php if ($row % 4 == 0 && $row != 0) : ?>
    </div>
        
    <div class="fixedly-row-<?php print $wp_query->post->ID;?> clearfix">
        
        <div class="fixedly-cell-<?php print $wp_query->post->ID;?>">
            
            <?php if ($rows_affected[$row]->type == "image") : ?>
            <a href="<?php print $rows_affected[$row]->screenshot;?>" class="fixedly-media-gallery" rel="group" title="<?php print $rows_affected[$row]->description;?>"><img src="<?php print $rows_affected[$row]->screenshot;?>" alt="<?php print $rows_affected[$row]->title;?>" /></a>
            <?php else : ?>
            <a href="<?php print $rows_affected[$row]->link;?>" class="fixedly-media-gallery" rel="group" title="<?php print $rows_affected[$row]->description;?>"><img src="<?php print $rows_affected[$row]->screenshot;?>" alt="<?php print $rows_affected[$row]->title;?>" /></a>
            <?php endif;?>
        </div>
    <?php else : ?>
        <div class="fixedly-cell-<?php print $wp_query->post->ID;?>">
             
            <?php if ($rows_affected[$row]->type == "image") : ?>
            <a href="<?php print $rows_affected[$row]->screenshot;?>" class="fixedly-media-gallery" rel="group" title="<?php print $rows_affected[$row]->description;?>"><img src="<?php print $rows_affected[$row]->screenshot;?>" alt="<?php print $rows_affected[$row]->title;?>" title="<?php print $rows_affected[$row]->title;?>" /></a>
            <?php else : ?>
            <a href="<?php print $rows_affected[$row]->link;?>" class="fixedly-media-gallery" rel="group" title="<?php print $rows_affected[$row]->description;?>"><img src="<?php print $rows_affected[$row]->screenshot;?>" alt="<?php print $rows_affected[$row]->title;?>" /></a>
            <?php endif;?>
        </div>
    <?php endif;?>
<?php endfor;?>
    </div>
</div>

<?php

        $rows_affected = $wpdb->get_results("SELECT $template_table_name.*
            FROM $template_table_name
                WHERE $template_table_name.id = '" . $template_id . "'");
?>

<?php if (!empty($rows_affected)) : ?>
<style type="text/css">
    <?php print update_template_option($rows_affected[0]->style, $rows_affected[0]->options, $wp_query->post->ID);?>
</style>

<script type="text/javascript">
    <?php print update_template_option($rows_affected[0]->script, $rows_affected[0]->options, $wp_query->post->ID);?>
</script>
<?php endif; ?>

<?php else : ?>
<p><em>[Fixedly Error #5]: You need to create a gallery and add media!</em></p>
<?php endif; ?>
