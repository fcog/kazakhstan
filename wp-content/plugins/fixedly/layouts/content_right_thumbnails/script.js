jQuery(document).ready(function(){

    jQuery(".fixedly-slider-cont-{%post-id%}").cycle({

        fx: "{%slider-effect%}",
        pause: true,
        random: {%slider-random%},
        timeout: {%slider-timeout%},
        speed: {%slider-speed%},
        startingSlide: {%slider-start%},
        delay: {%slider-delay%},
        prev: ".fixedly-nav-prev-{%post-id%} a",
        next: ".fixedly-nav-next-{%post-id%} a",
        pager: ".fixedly-pager-outer-{%post-id%}",

        pagerAnchorBuilder: function(idx, slide) {

            return '.fixedly-pager-outer-{%post-id%} li:eq(' + idx + ') a';
        }
    });

    jQuery(".fixedly-pager-outer-{%post-id%}").cycle({

        fx: "scrollHorz",
        timeout: 0,
        speed: 1000,
        prev: ".fixedly-pager-nav-prev-{%post-id%} a",
        next: ".fixedly-pager-nav-next-{%post-id%} a",
    });

    jQuery(".fixedly-meta-cont-{%post-id%}").cycle({

        fx: "fade",
        pause: true,
        timeout: 0,
        speed: 0,
        prev: ".fixedly-nav-prev-{%post-id%} a",
        next: ".fixedly-nav-next-{%post-id%} a",
        pager: ".fixedly-pager-outer-{%post-id%}",

        pagerAnchorBuilder: function(idx, slide) {

            return '.fixedly-pager-outer-{%post-id%} li:eq(' + idx + ') a';
        }
    });
});
