<?php

    global $wpdb;
    global $wp_query;

    $media_table_name    = FIXEDLY_MEDIA_DB_TABLE;
    $gallery_table_name  = FIXEDLY_GALLERY_DB_TABLE;
    $template_table_name = FIXEDLY_TEMPLATE_DB_TABLE;

    $rows_affected = $wpdb->get_results("SELECT $media_table_name.*
        FROM $media_table_name, $gallery_table_name
            WHERE $media_table_name.gallery_id = '" . $gallery_id . "'
                AND $media_table_name.status = 'visible'
                    AND $gallery_table_name.status = 'visible'
                        AND $media_table_name.gallery_id = $gallery_table_name.id
                            AND $gallery_table_name.status = 'visible'
                                ORDER BY order_id ASC;");
?>

<?php if (!empty($rows_affected)) : ?>

<div class="fixedly-cont-<?php print $wp_query->post->ID;?> clearfix">

    <div class="fixedly-slider-cont-<?php print $wp_query->post->ID;?>">

        <?php for ($row = 0; $row < sizeof($rows_affected); $row++) : ?>

        <div class="fixedly-slide-<?php print $wp_query->post->ID;?>">

            <?php if ($rows_affected[$row]->type == "image") : ?>
            <a href="<?php print $rows_affected[$row]->link;?>"><img src="<?php print $rows_affected[$row]->screenshot;?>" alt="" title="<?php print $rows_affected[$row]->title;?>" /></a>
            <?php else : ?>
            <iframe src="<?php print $rows_affected[$row]->link;?>?wmode=transparent&autohide=1&hd=1&modestbranding=1" height="" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
            <?php endif;?>
        </div>
        <?php endfor;?>
    </div>

    <div class="fixedly-meta-nav-cont-<?php print $wp_query->post->ID;?>">

        <div class="fixedly-meta-cont-<?php print $wp_query->post->ID;?>">

            <?php for ($row = 0; $row < sizeof($rows_affected); $row++) : ?>

            <div class="fixedly-meta-<?php print $wp_query->post->ID;?>">
                <div class="fixedly-meta-title-<?php print $wp_query->post->ID;?>"><?php print $rows_affected[$row]->title;?></div>
                <div class="fixedly-meta-description-<?php print $wp_query->post->ID;?>"><?php print $rows_affected[$row]->description;?></div>
            </div>
            <?php endfor;?>
        </div>

        <div class="fixedly-nav-<?php print $wp_query->post->ID;?>">
            <div class="fixedly-nav-prev-<?php print $wp_query->post->ID;?>"><a href="javascript:void(0);">&laquo;</a></div>
            <div class="fixedly-nav-next-<?php print $wp_query->post->ID;?>"><a href="javascript:void(0);">&raquo;</a></div>
        </div>
    </div>
</div>

<?php

        $rows_affected = $wpdb->get_results("SELECT $template_table_name.*
            FROM $template_table_name
                WHERE $template_table_name.id = '" . $template_id . "'");
?>

<?php if (!empty($rows_affected)) : ?>
<style type="text/css">
    <?php print update_template_option($rows_affected[0]->style, $rows_affected[0]->options, $wp_query->post->ID);?>
</style>

<script type="text/javascript">
    <?php print update_template_option($rows_affected[0]->script, $rows_affected[0]->options, $wp_query->post->ID);?>
</script>
<?php endif; ?>

<?php else : ?>
<p><em>[Fixedly Error #5]: You need to create a gallery and add media!</em></p>
<?php endif; ?>
