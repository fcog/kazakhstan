<?php
/**
    Plugin Name: Fixedly Media Gallery
    Plugin URI: http://www.fixedly.net/
    Description: Create and integrate <strong>easily and quickly</strong> your <strong>video, image, or slideshow gallery</strong> into your WordPress pages/posts. Check out our website for <a href="http://www.fixedly.net/">usage, demos &amp; screencasts</a>.
    Version: 1.3.1
    Author: Krasen Slavov
    Author URI: http://www.thechoppr.com/
    License: GPL2

    Copyright 2012 Krasen Slavov (email : hello@thechoppr.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/**
 * Define some global plugin variables.
 */

    global $wpdb;
    global $fixedly_db_version;

    $fixedly_db_version = "0.3";

    define("FIXEDLY_GALLERY_DB_TABLE",  $wpdb->prefix . "fixedly_gallery");
    define("FIXEDLY_MEDIA_DB_TABLE",    $wpdb->prefix . "fixedly_media");
    define("FIXEDLY_TEMPLATE_DB_TABLE", $wpdb->prefix . "fixedly_template");

    define("FIXEDLY_PLUGIN_VERSION",    "1.3.1");
    define("FIXEDLY_PLUGIN_URL",        plugin_dir_url( __FILE__ ));
    define("FIXEDLY_PLUGIN_DIR",        plugin_dir_path( __FILE__ ));
    
    require_once(ABSPATH . "wp-admin/includes/upgrade.php");
    
/**
  * During activation create and initialize database tables and insert default data.
  */

    register_activation_hook(__FILE__, "fixedly_install");

    function fixedly_install() {

        global $wpdb;
        global $fixedly_db_version;
        
        $templates_arr = array(
            "default",
            "default_thumbnails",
            "content_left",
            "content_right",
            "content_top",
            "content_bottom",
            "content_left_thumbnails",
            "content_right_thumbnails",
            "gallery"
        );

        $fixedly_db_version_installed = get_option("fixedly_db_version");

        // do this to remove older version database tables (for version 0.3)
        if ($fixedly_db_version_installed != $fixedly_db_version) {

            if ($fixedly_db_version_installed == "0.1" || $fixedly_db_version_installed == "0.2") {

                $wpdb->query("DROP TABLE " . FIXEDLY_TEMPLATE_DB_TABLE . ";");
                $wpdb->query("DROP TABLE " . FIXEDLY_MEDIA_DB_TABLE . ";");
                $wpdb->query("DROP TABLE " . FIXEDLY_GALLERY_DB_TABLE . ";");
        
                update_option("fixedly_db_version", $fixedly_db_version);
            }

        
            $fixedly_create_sql = "

                CREATE TABLE " . FIXEDLY_GALLERY_DB_TABLE . " (
                    id int(11) NOT NULL AUTO_INCREMENT,
                    title VARCHAR(128) DEFAULT '' NOT NULL,
                    description VARCHAR(255) DEFAULT '' NOT NULL,
                    type ENUM('image','video','mixed') DEFAULT 'image',
                    status ENUM('visible','hidden','deleted') DEFAULT 'visible',
                    timestamp DATETIME DEFAULT '0000-00-00 00:00:00' NOT NULL,
                    template_id int(11) NOT NULL,
                    UNIQUE KEY id (id));
                
                CREATE TABLE " . FIXEDLY_TEMPLATE_DB_TABLE . " (
                    id int(11) NOT NULL AUTO_INCREMENT,
                    title VARCHAR(128) DEFAULT '' NOT NULL,
                    style text NOT NULL,
                    script text NOT NULL,
                    options text NOT NULL,
                    timestamp DATETIME DEFAULT '0000-00-00 00:00:00' NOT NULL,
                    UNIQUE KEY id (id));

                CREATE TABLE " . FIXEDLY_MEDIA_DB_TABLE . " (
                    id int(11) NOT NULL AUTO_INCREMENT,
                    title VARCHAR(128) DEFAULT '' NOT NULL,
                    link VARCHAR(255) DEFAULT '' NOT NULL,
                    description text NOT NULL,
                    screenshot VARCHAR(255) DEFAULT '' NOT NULL,
                    type ENUM('image','vimeo','youtube','dailymotion') DEFAULT 'image',
                    status ENUM('visible','hidden','deleted') DEFAULT 'visible',
                    timestamp DATETIME DEFAULT '0000-00-00 00:00:00' NOT NULL,
                    gallery_id int(11) NOT NULL,
                    order_id int(11) NOT NULL,
                    UNIQUE KEY id (id));";

            add_option("fixedly_db_version", $fixedly_db_version);
            
            dbDelta($fixedly_create_sql);
            
            foreach ($templates_arr as $template_name)  {
            
                $template_dir = FIXEDLY_PLUGIN_DIR . "layouts/" . $template_name . "/";
                $options_tmpl = "";

                if (is_dir($template_dir)) {
                
                    $style_tmpl  = sanitize_text_field(file_get_contents($template_dir . "style.css"));
                    $script_tmpl = sanitize_text_field(file_get_contents($template_dir . "script.js"));
                
                    if (is_file($template_dir . "options.txt"))
                    $options_tmpl = sanitize_text_field(file_get_contents($template_dir . "options.txt"));
                
                    $wpdb->query("INSERT INTO " . FIXEDLY_TEMPLATE_DB_TABLE . "
                        SET title = '" . $template_name . "',
                            style = '" . mysql_real_escape_string($style_tmpl) . "',
                            script = '" . mysql_real_escape_string($script_tmpl) . "',
                            options = '" . mysql_real_escape_string($options_tmpl) . "',
                            timestamp = NOW();");
                }
            }
        }

    }
    
    register_uninstall_hook(__FILE__, "fixedly_delete");
    
    function fixedly_delete() {
    
        global $wpdb;
        global $fixedly_db_version;
        
        $wpdb->query("DROP TABLE " . FIXEDLY_TEMPLATE_DB_TABLE . ";");
        $wpdb->query("DROP TABLE " . FIXEDLY_MEDIA_DB_TABLE . ";");
        $wpdb->query("DROP TABLE " . FIXEDLY_GALLERY_DB_TABLE . ";");

        update_option("fixedly_db_version", "0.0");
    }

/**
  * Add plugin shortcode, init and loading functions.
  */

    add_shortcode("fixedly-media-gallery", "fixedly_init");

    function fixedly_init($args) {

        if (!empty($args["id"])) {

            if ($gallery = fixedly_selected_gallery($args["id"])) {
            
                (empty($args["template_id"])) ? $template_id = $gallery->template_id : $template_id = $args["template_id"];

                if (function_exists("fixedly_media_gallery")) {

                    return fixedly_media_gallery($args["id"], $template_id);
                }
                else
                    return "<p><em>[Fixedly Error #3]: Please enable your Fixedly Media Gallery plugin!</em></p>";
            }
            else
                return "<p><em>[Fixedly Error #2]: The gallery you try to display is deleted or doesn't exist!</em></p>";
        }
        else
            return "<p><em>[Fixedly Error #1]: You need to specify the 'id' for your gallery!</em></p>";
    }
    
    function fixedly_selected_gallery($gallery_id) {

        if (!empty($gallery_id)) {

            global $wpdb;

            $rows_affected = $wpdb->get_results("SELECT * FROM " . FIXEDLY_GALLERY_DB_TABLE . "
                WHERE status != 'deleted'
                    AND id = '" . $gallery_id . "'");

            if (sizeof($rows_affected) > 0)
                return $rows_affected[0];
        }

        return false;
    }

    function fixedly_media_gallery($gallery_id, $template_id) {

        if (!empty($template_id)) {
        
            global $wpdb;
            
            $rows_affected = $wpdb->get_results("SELECT * FROM " . FIXEDLY_TEMPLATE_DB_TABLE . "
                WHERE id = '" . $template_id . "'");
                    
            $template_dir = str_replace(" ", "_", $rows_affected[0]->title);

            if (is_dir(FIXEDLY_PLUGIN_DIR . "layouts/" . $template_dir . "/")) {

                ob_start();
                require(FIXEDLY_PLUGIN_DIR . "layouts/" . $template_dir . "/index.php");
                $data = ob_get_clean();
                ob_flush();
                
                return $data;
            }
            else
                return "<p><em>[Fixedly Error #4]: The template name you entered cannot be found!</em></p>";
        }
        else
            return false;
    }

/**
  * Add stylesheet and javascript files to the admin & template headers.
  */

    add_action("wp_enqueue_scripts", "load_fixedly_theme_header");
    add_action("admin_enqueue_scripts", "load_fixedly_admin_header");

    if (isset($_GET["page"]) && ($_GET["page"] == "manage_media")) {

        add_action("admin_enqueue_scripts", "load_fixedly_media_upload_header");
    }
    
    function load_fixedly_theme_header() {

        wp_register_script("jquery.cycle.all-2.99.js", FIXEDLY_PLUGIN_URL . "assets/scripts/jquery.cycle.all-2.99.js", array("jquery"));
        wp_enqueue_script("jquery.cycle.all-2.99.js");

        wp_register_script("jquery.fancybox-2.1.pack.js", FIXEDLY_PLUGIN_URL . "assets/scripts/jquery.fancybox-2.1.pack.js", array("jquery"));
        wp_enqueue_script("jquery.fancybox-2.1.pack.js");
        
        wp_register_script("jquery.fancybox-media.js", FIXEDLY_PLUGIN_URL . "assets/scripts/jquery.fancybox-media.js", array("jquery"));
        wp_enqueue_script("jquery.fancybox-media.js");
        
        wp_register_style("jquery.fancybox.css", FIXEDLY_PLUGIN_URL . "assets/styles/jquery.fancybox.css");
        wp_enqueue_style("jquery.fancybox.css");
        
        wp_register_style("fixedly.css", FIXEDLY_PLUGIN_URL . "assets/styles/fixedly.css");
        wp_enqueue_style("fixedly.css");
    }

    function load_fixedly_admin_header() {
    
        wp_register_script("jquery.validate-1.9.min.js", FIXEDLY_PLUGIN_URL . "assets/scripts/jquery.validate-1.9.min.js", array("jquery"));
        wp_enqueue_script("jquery.validate-1.9.min.js");

        wp_register_script("jquery.jeditable-1.7.1.js", FIXEDLY_PLUGIN_URL . "assets/scripts/jquery.jeditable-1.7.1.js", array("jquery"));
        wp_enqueue_script("jquery.jeditable-1.7.1.js");
        
        wp_register_script("jquery.colorpicker.js", FIXEDLY_PLUGIN_URL . "assets/scripts/jquery.colorpicker.js", array("jquery"));
        wp_enqueue_script("jquery.colorpicker.js");
        
        wp_register_style("jquery.colorpicker.css", FIXEDLY_PLUGIN_URL . "assets/styles/jquery.colorpicker.css");
        wp_enqueue_style("jquery.colorpicker.css");

        wp_register_script("jquery.fixedly-1.3.js", FIXEDLY_PLUGIN_URL . "assets/scripts/jquery.fixedly-1.3.js", array("jquery", "media-upload", "thickbox"));
        wp_enqueue_script("jquery.fixedly-1.3.js");

        wp_register_style("default.css", FIXEDLY_PLUGIN_URL . "assets/styles/default.css");
        wp_enqueue_style("default.css");
    }

    function load_fixedly_media_upload_header() {

        wp_enqueue_script("media-upload");
        wp_enqueue_script("thickbox");
        wp_enqueue_style("thickbox");
    }
    
/**
 * Create and add WordPress admin panel menu tabs for the plugin.
 */

    add_action("admin_menu", "fixedly_menu");

    function fixedly_menu() {

        add_menu_page("Fixedly Media Gallery",
            "Fixedly Media",
            "administrator",
            "fixedly/fixedly.php",
            "fixedly_manage_gallery"
        );
        
        add_submenu_page("fixedly/fixedly.php",
            "Fixedly - Manage your Galleries",
            "My Galleries",
            "administrator",
            "fixedly/fixedly.php",
            "fixedly_manage_gallery"
        );
        
        add_submenu_page("fixedly/fixedly.php",
            "Fixedly - Manage your Templates",
            "My Templates",
            "administrator",
            "manage_template",
            "fixedly_manage_template"
        );

        add_submenu_page("fixedly/fixedly.php",
            "Fixedly - Manage your Media",
            "Media Library",
            "administrator",
            "manage_media",
            "fixedly_manage_media"
        );
    }
    
/**
  * Create ajax actions to update gallery/media title, description, link, template and type.
  */
  
    if ($_GET["do"] == "save_gallery_title") {

        if (!empty($_POST["id"]) && !empty($_POST["value"])) {

            $title_santzd = sanitize_text_field($_POST["value"]);

            $wpdb->update(FIXEDLY_GALLERY_DB_TABLE,
                array("title" => $title_santzd),
                array("id" => $_POST["id"]),
                array("%s"),
                array("%d")
            );
        }

        print $title_santzd;
        exit;
    }
    
    if ($_GET["do"] == "save_gallery_description") {

        if (!empty($_POST["id"]) && !empty($_POST["value"])) {
        
            $description_santzd = sanitize_text_field($_POST["value"]);

            $wpdb->update(FIXEDLY_GALLERY_DB_TABLE,
                array("description" => $description_santzd),
                array("id" => $_POST["id"]),
                array("%s"),
                array("%d")
            );
        }

        print $description_santzd;
        exit;
    }
    
    if ($_GET["do"] == "save_gallery_type") {

        if (!empty($_POST["id"]) && !empty($_POST["value"])) {

            $type_santzd = sanitize_text_field($_POST["value"]);

            $wpdb->update(FIXEDLY_GALLERY_DB_TABLE,
                array("type" => $type_santzd),
                array("id" => $_POST["id"]),
                array("%s"),
                array("%d")
            );
        }

        print $type_santzd;
        exit;
    }
    
    if ($_GET["do"] == "load_gallery_templates") {

        $rows_affected = $wpdb->get_results("SELECT id, title
            FROM " . FIXEDLY_TEMPLATE_DB_TABLE . "
                GROUP BY title");

        for ($row = 0; $row < sizeof($rows_affected); $row++) {

            $templates[$rows_affected[$row]->id] = $rows_affected[$row]->title;
        }

        $templates["selected"] = "default";

        print json_encode($templates);
        exit;
    }
    
    if ($_GET["do"] == "save_gallery_template") {

        if (!empty($_POST["id"]) && !empty($_POST["value"])) {

            $template_id_santzd = sanitize_text_field($_POST["value"]);

            $wpdb->update(FIXEDLY_GALLERY_DB_TABLE,
                array("template_id" => $template_id_santzd),
                array("id" => $_POST["id"]),
                array("%s"),
                array("%d")
            );
        }

        print fixedly_get_template_name($template_id_santzd);
        exit;
    }
    
    if ($_GET["do"] == "save_gallery_status") {

        if (!empty($_POST["id"]) && !empty($_POST["value"])) {

            $status_santzd = sanitize_text_field($_POST["value"]);

            $wpdb->update(FIXEDLY_GALLERY_DB_TABLE,
                array("status" => $status_santzd),
                array("id" => $_POST["id"]),
                array("%s"),
                array("%d")
            );
        }

        print $status_santzd;
        exit;
    }
    
    if ($_GET["do"] == "save_media_order") {

        if (!empty($_POST["id"]) && !empty($_POST["value"])) {

            $order_santzd = sanitize_text_field($_POST["value"]);

            $wpdb->update(FIXEDLY_MEDIA_DB_TABLE,
                array("order_id" => $order_santzd),
                array("id" => $_POST["id"]),
                array("%s"),
                array("%d")
            );
        }

        print $order_santzd;
        exit;
    }
    
    if ($_GET["do"] == "save_media_title") {

        if (!empty($_POST["id"]) && !empty($_POST["value"])) {

            $title_santzd = sanitize_text_field($_POST["value"]);

            $wpdb->update(FIXEDLY_MEDIA_DB_TABLE,
                array("title" => $title_santzd),
                array("id" => $_POST["id"]),
                array("%s"),
                array("%d")
            );
        }

        print $title_santzd;
        exit;
    }
    
    if ($_GET["do"] == "save_media_link") {

        if (!empty($_POST["id"]) && !empty($_POST["value"])) {

            $link_santzd = sanitize_text_field($_POST["value"]);

            $wpdb->update(FIXEDLY_MEDIA_DB_TABLE,
                array("link" => $link_santzd),
                array("id" => $_POST["id"]),
                array("%s"),
                array("%d")
            );
        }

        print $link_santzd;
        exit;
    }
    
    if ($_GET["do"] == "save_media_description") {

        if (!empty($_POST["id"]) && !empty($_POST["value"])) {

            $description_santzd = sanitize_text_field($_POST["value"]);

            $wpdb->update(FIXEDLY_MEDIA_DB_TABLE,
                array("description" => $description_santzd),
                array("id" => $_POST["id"]),
                array("%s"),
                array("%d")
            );
        }

        print $description_santzd;
        exit;
    }
    
    if ($_GET["do"] == "save_media_status") {

        if (!empty($_POST["id"]) && !empty($_POST["value"])) {

            $status_santzd = sanitize_text_field($_POST["value"]);

            $wpdb->update(FIXEDLY_MEDIA_DB_TABLE,
                array("status" => $status_santzd),
                array("id" => $_POST["id"]),
                array("%s"),
                array("%d")
            );
        }

        print $status_santzd;
        exit;
    }
    
/**
  * Below are fuctions that will manage GALLERIES.
  */
    
    function fixedly_manage_gallery() {
    
        $success_text = "";
        
        if (isset($_POST["action"]) && $_POST["action"] == "add_new_gallery") {
        
            if (fixedly_add_gallery($_POST["title"], $_POST["description"], $_POST["type"], $_POST["template"]))
                $success_text = "New gallery added successfuly!";
        }
    
        require_once FIXEDLY_PLUGIN_DIR . "pages/create_gallery_page.php";
        
        print_by_line();
    }
    
    function fixedly_add_gallery($title, $description, $type, $template) {

        global $wpdb;

        $title_santzd       = sanitize_text_field($title);
        $description_santzd = sanitize_text_field($description);
        $type_santzd        = sanitize_text_field($type);
        $template_santzd    = sanitize_text_field($template);

        if ($wpdb->insert(FIXEDLY_GALLERY_DB_TABLE,
            array("title"       => $title_santzd,
                "description"   => $description_santzd,
                "type"          => $type_santzd,
                "status"        => "visible",
                "timestamp"     => current_time("mysql"),
                "template_id"   => $template_santzd))) {

            return true;
        }

        return false;
    }
    
    function fixedly_select_galleries() {

        global $wpdb;

        $gallery_table_name     = FIXEDLY_GALLERY_DB_TABLE;
        $media_table_name       = FIXEDLY_MEDIA_DB_TABLE;
        
        $rows_affected = $wpdb->get_results("SELECT $gallery_table_name.*,
            (SELECT COUNT(*) FROM $media_table_name WHERE $gallery_table_name.id = $media_table_name.gallery_id AND $media_table_name.status = 'visible') AS num_items
                FROM $gallery_table_name
                WHERE $gallery_table_name.status != 'deleted'");

        for ($row = 0; $row < sizeof($rows_affected); $row++) {

            print '<tr>
                <td class="acenter"><a href="?page=fixedly/fixedly.php&action=manage_gallery&gid=' . $rows_affected[$row]->id . '">' . $rows_affected[$row]->id . '</a></td>
                <td class="acenter">' . $rows_affected[$row]->num_items . '</td>
                <td><abbr class="edit-gallery-title" id="' . $rows_affected[$row]->id . '">' . $rows_affected[$row]->title . '</abbr></td>
                <td><abbr class="edit-gallery-description" id="' . $rows_affected[$row]->id . '">' . $rows_affected[$row]->description . '</abbr></td>
                <td class="acenter" nowrap><abbr class="edit-gallery-type" id="' . $rows_affected[$row]->id . '">' . $rows_affected[$row]->type . '</abbr></td>
                <td class="acenter" nowrap><abbr class="edit-gallery-template" id="' . $rows_affected[$row]->id . '">' . fixedly_get_template_name($rows_affected[$row]->template_id) . '</abbr></td>
                <td class="acenter" nowrap><abbr class="edit-gallery-status" id="' . $rows_affected[$row]->id . '">' . $rows_affected[$row]->status . '</abbr></td>
                <td class="acenter" nowrap>' . mysql2date('M dS, Y', $rows_affected[$row]->timestamp) . '</td>
            </tr>';
        }
    }
    
    function fixedly_get_gallery_list() {
    
        global $wpdb;

        $rows_affected = $wpdb->get_results("SELECT id, title
            FROM " . FIXEDLY_GALLERY_DB_TABLE . "
                WHERE status != 'deleted'");

        for ($row = 0; $row < sizeof($rows_affected); $row++) {

            print '<option value="' . $rows_affected[$row]->id . '">' . $rows_affected[$row]->title . '</option>';
        }
    }
    
/**
  * Below are fuctions that will manage TEMPLATES.
  */

    function fixedly_manage_template() {
    
        global $wpdb;

        $success_text = "";
        
        if (isset($_POST["action"]) && $_POST["action"] == "update_template") {

            if (fixedly_update_template($_POST["template_id"], $_POST))
                $success_text = "Template was updated successfuly!";
        }

        if (isset($_GET["template_id"])) {

            $template_id_santzd = sanitize_text_field($_GET["template_id"]);
        
            $rows_affected = $wpdb->get_results("SELECT *
                FROM " . FIXEDLY_TEMPLATE_DB_TABLE . "
                    WHERE id = '" . $template_id_santzd . "'");
                    
            $arr = explode(";", $rows_affected[0]->options);
            
            foreach ($arr as $val) {

                $arr2 = explode(":", trim($val));
                $output[$arr2[0]] = $arr2[1];
            }
        }

        require_once FIXEDLY_PLUGIN_DIR . "pages/create_template_page.php";
        
        print_by_line();
    }
    
    function fixedly_update_template($template_id, $options) {
    
        global $wpdb;
    
        $opt_text = "";
    
        foreach ($options as $var => $val) {
        
            if (!in_array($var, array("action", "template", "template_id")))
                $opt_text .= $var . ":" . $val . ";";
        }
            
        if ($wpdb->update(FIXEDLY_TEMPLATE_DB_TABLE,
                array("options" => $opt_text),
                array("id" => $template_id),
                array("%s"),
                array("%d")))
            return true;
    }
    
    function fixedly_get_template_effects($selected = 0) {

        $effects = array("blindX",
            "blindY",
            "blindZ",
            "cover",
            "curtainX",
            "curtainY",
            "fade",
            "fadeZoom",
            "growX",
            "growY",
            "none",
            "scrollUp",
            "scrollDown",
            "scrollLeft",
            "scrollRight",
            "scrollHorz",
            "scrollVert",
            "shuffle",
            "slideX",
            "slideY",
            "toss",
            "turnUp",
            "turnDown",
            "turnLeft",
            "turnRight",
            "uncover",
            "wipe",
            "zoom");

        for ($i = 0; $i < sizeof($effects); $i++) {

            if (isset($selected)) {
            
                if ($effects[$i] == $selected)
                    print '<option value="' . $effects[$i] . '" selected>' . $effects[$i] . '</option>';
                else
                    print '<option value="' . $effects[$i] . '">' . $effects[$i] . '</option>';
            }
            else
                print '<option value="' . $effects[$i] . '">' . $effects[$i] . '</option>';
        }
    }
    
    function fixedly_get_template_name($template_id) {

        global $wpdb;

        if (isset($template_id)) {

            $rows_affected = $wpdb->get_results("SELECT title
                FROM " . FIXEDLY_TEMPLATE_DB_TABLE . "
                    WHERE id = '" . $template_id . "'");

            return $rows_affected[0]->title;
        }

        return false;
    }

    function fixedly_get_template_list($selected = 0) {

        global $wpdb;

        $rows_affected = $wpdb->get_results("SELECT id, title
            FROM " . FIXEDLY_TEMPLATE_DB_TABLE);

        for ($row = 0; $row < sizeof($rows_affected); $row++) {

            if (isset($selected)) {
            
                if ($rows_affected[$row]->id ==  $selected)
                    print '<option value="' . $rows_affected[$row]->id . '" selected>' . $rows_affected[$row]->title . '</option>';
                else
                    print '<option value="' . $rows_affected[$row]->id . '">' . $rows_affected[$row]->title . '</option>';
            }
            else
                print '<option value="' . $rows_affected[$row]->id . '">' . $rows_affected[$row]->title . '</option>';
        }
    }
    
    function update_template_option($code, $options, $post_id) {
    
        $code = str_replace("{%post-id%}", $post_id, $code);
    
        $arr = explode(";", $options);
        
        foreach ($arr as $val) {
        
            $arr2 = explode(":", trim($val));
            $code = str_replace("{%" . $arr2[0] . "%}", $arr2[1], $code);
        }
        
        return $code;
    }
    
    function get_template_option($options, $get_val) {
    
        $arr = explode(";", $options);

        foreach ($arr as $val) {
        
            $arr2 = explode(":", trim($val));

            if ($get_val == $arr2[0])
                return $arr2[1];
        }

        return true;
    }
    
/**
  * Below are fuctions that will manage MEDIA.
  */
    function fixedly_manage_media() {
    
        $success_text = "";

        if (isset($_POST["action"]) && $_POST["action"] == "add_new_media") {

            if ($error_text = fixedly_add_media($_POST["gid"], $_POST["title"], $_POST["description"], $_POST["link"], $_POST["type"], $_POST["screenshot"])) {

                if (is_bool($error_text) == true)
                    $success_text = "New media added successfuly!";
                else
                    $success_text = $error_text;
            }
        }
    
        require_once FIXEDLY_PLUGIN_DIR . "pages/create_media_page.php";
        
        print_by_line();
    }
    
    function fixedly_add_media($gallery_id, $title, $description, $link, $type, $screenshot) {
    
        global $wpdb;

        $gallery_id_santzd  = sanitize_text_field($gallery_id);
        
        for ($row = 0; $row < sizeof($title); $row++) {

            if (empty($title[$row]) && empty($type[$row]) && empty($description[$row]))
                return "One or more required fields are empty!";
        }

        for ($row = 0; $row < sizeof($title); $row++) {

            if (!empty($title[$row]) && !empty($type[$row]) && !empty($description[$row])) {

                $title_santzd       = sanitize_text_field($title[$row]);
                $description_santzd = sanitize_text_field($description[$row]);
                $link_santzd        = sanitize_text_field($link[$row]);
                $type_santzd        = sanitize_text_field($type[$row]);
                $screenshot_santzd  = sanitize_text_field($screenshot[$row]);
                
                if ($type_santzd == "image") {

                    $wpdb->insert(FIXEDLY_MEDIA_DB_TABLE,
                        array("title"       => $title_santzd,
                            "link"          => $link_santzd,
                            "description"   => $description_santzd,
                            "screenshot"    => $screenshot_santzd,
                            "type"          => $type_santzd,
                            "status"        => "visible",
                            "timestamp"     => current_time("mysql"),
                            "gallery_id"    => $gallery_id_santzd,
                            "order_id"      => 0));
                }
                else { // video
                
                    if ($type_santzd == "vimeo" ) { // vimeo
                    
                        if (is_numeric($link_santzd)) {

                            $hash = @unserialize(file_get_contents("http://vimeo.com/api/v2/video/" . $link_santzd . ".php"));
                            $screenshot_santzd = $hash[0]["thumbnail_medium"];
                            $link_santzd = "http://player.vimeo.com/video/" . $link_santzd;
                        }
                        else
                            return "One or more of your Vimeo video links didn't match the correct format.";
                    }

                    if ($type_santzd == "youtube") { // youtube
                    
                        if ((strlen($link_santzd) == 11)) {

                            $screenshot_santzd = "http://img.youtube.com/vi/" . $link_santzd . "/0.jpg";
                            $link_santzd = "http://www.youtube.com/embed/" . $link_santzd;
                        }
                        else
                            return "One or more of your YouTube video links didn't match the correct format.";
                    }

                    if ($type_santzd == "dailymotion") { // dailymotion

                        $screenshot_santzd = "http://www.dailymotion.com/thumbnail/video/" . $link_santzd;
                        $link_santzd = "http://www.dailymotion.com/embed/video/" . $link_santzd;
                    }

                    $wpdb->insert(FIXEDLY_MEDIA_DB_TABLE,
                        array("title"     => $title_santzd,
                            "link"        => $link_santzd,
                            "description" => $description_santzd,
                            "screenshot"  => $screenshot_santzd,
                            "type"        => $type_santzd,
                            "status"      => "visible",
                            "timestamp"   => current_time("mysql"),
                            "gallery_id"  => $gallery_id_santzd));
                }
            }
        }

        return true;
    }
    
    function fixedly_select_media($gallery_id, $current_page) {
    
        global $wpdb;

        $show_entries_per_page  = 10;
        $start_from             = $current_page * $show_entries_per_page;
        
        $rows_affected = $wpdb->get_results("SELECT *
            FROM " . FIXEDLY_MEDIA_DB_TABLE . "
                WHERE status != 'deleted'
                    AND gallery_id = '" . $gallery_id . "'
                        LIMIT " . $start_from . ", " . $show_entries_per_page);

        for ($row = 0; $row < sizeof($rows_affected); $row++) {

            print '<tr>
                <td class="acenter">' . $rows_affected[$row]->id . '</td>
                <td class="acenter"><abbr class="edit-media-order" id="' . $rows_affected[$row]->id . '">' . $rows_affected[$row]->order_id .'</abbr></td>
                <td class="acenter"><img src="' . $rows_affected[$row]->screenshot . '" alt="" width="80" height="80" /></td>
                <td class="aleft"><abbr class="edit-media-title" id="' . $rows_affected[$row]->id . '">' . $rows_affected[$row]->title . '</abbr></td>
                <td class="aleft"><abbr class="edit-media-link" id="' . $rows_affected[$row]->id . '">' . $rows_affected[$row]->link . '</abbr></td>
                <td class="aleft"><abbr class="edit-media-description" id="' . $rows_affected[$row]->id . '">' . $rows_affected[$row]->description .'</abbr></td>
                <td class="acenter" nowrap>' . $rows_affected[$row]->type . '</td>
                <td class="acenter" nowrap><abbr class="edit-media-status" id="' . $rows_affected[$row]->id . '">' . $rows_affected[$row]->status . '</abbr></td>
                <td class="acenter" nowrap>' . mysql2date('M dS, Y', $rows_affected[$row]->timestamp) . '</td>
            </tr>';
        }
    }
    
    function fixedly_paginate_media($gallery_id, $current_page) {

        global $wpdb;
        
        $media_table_name = FIXEDLY_MEDIA_DB_TABLE;

        $show_entries_per_page = 10;

        ($current_page == "" || $current_page == 0) ? $current_page = 1 : "";

        if (!empty($current_page) && !empty($gallery_id)) {

            $rows_affected = $wpdb->get_results("SELECT *
                FROM " . $media_table_name . "
                    WHERE status != 'deleted'
                        AND gallery_id = '" . $gallery_id . "'");

            $total_entries  = sizeof($rows_affected);
            $number_pages   = ceil($total_entries / $show_entries_per_page);
            $prev_page      = $current_page - 1;

            (($current_page + 1) >= $number_pages) ? $next_page = $number_pages - 1 : $next_page = $current_page + 1;

            $last_page  = $number_pages - 1;
            $first_page = 0;

            print '<a href="?page=fixedly/fixedly.php&action=manage_gallery&gid=' . $gallery_id . '&cpage=' . $first_page . '" class="button-primary">First Page</a> &nbsp;
                <a href="?page=fixedly/fixedly.php&action=manage_gallery&gid=' . $gallery_id . '&cpage=' . $prev_page . '" class="button-primary">< Previous Page</a> &nbsp;
                <a href="?page=fixedly/fixedly.php&action=manage_gallery&gid=' . $gallery_id . '&cpage=' . $next_page . '" class="button-primary">Next Page ></a> &nbsp;
                <a href="?page=fixedly/fixedly.php&action=manage_gallery&gid=' . $gallery_id . '&cpage=' . $last_page . '" class="button-primary">Last Page</a> &nbsp;';
        }
    }
    
/**
  * Miscellaneous functions
  */

function print_by_line() {

    print '<p class="fixedly-by-line"><em>Fixedly Media Gallery ' . FIXEDLY_PLUGIN_VERSION . '</em> is coded by <a href="http://www.thechoppr.com">Krasen Slavov</a>.</p>';
}

?>